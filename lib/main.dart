import 'package:findix_app/services/analytic_service.dart';
import 'package:findix_app/utils/constants.dart';
import 'package:flutter/material.dart';
import 'package:findix_app/Pages/home-page.dart';
import 'package:flutter_easyloading/flutter_easyloading.dart';
import 'package:findix_app/Pages/auth/register-page.dart';
import 'package:findix_app/utils/storage.dart' as storage;
import 'package:findix_app/utils/keys.dart' as storageKeys;
import 'locator.dart';

void main() {
  setupLocator();
  runApp(MyApp());
  configLoading();
}

class MyApp extends StatefulWidget {
  // This widget is the root of your application.
  @override
  _MyAppState createState() => _MyAppState();
}

class _MyAppState extends State<MyApp> {
  String isLoggedIn = "false";
  bool isLoading = true;

  getStatus() async {
    var loggedIn = await storage.read(storageKeys.isLoggedIn);
    setState(() {
      isLoggedIn = loggedIn ?? "false";
      isLoading = false;
    });
  }

  @override
  void initState() {
    super.initState();
    getStatus();
  }

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      
      routes: {
        '/home': (context) => HomePage(),
      },
      title: 'Findix App',
      navigatorObservers: [
        locator<AnalyticsService>().getAnalyticsObserver(),
      ],
      home: isLoading
          ? Center(child: CircularProgressIndicator())
          : isLoggedIn == "true"
              ? HomePage()
              : RegisterPage(),
      builder: EasyLoading.init(),
      theme: ThemeData(
        appBarTheme: AppBarTheme(brightness: Brightness.dark),
        bottomSheetTheme: BottomSheetThemeData(
          backgroundColor: Colors.white,
        ),
        textTheme: TextTheme(
          bodyText1: TextStyle(
            color: secondaryBlue,
            fontSize: 16,
            fontWeight: FontWeight.w400,
          ),
          bodyText2: TextStyle(
            color: secondaryBlue,
            fontSize: 16,
            fontWeight: FontWeight.w400,
          ),
        ),
      ),
    );
  }
}

void configLoading() {
  EasyLoading.instance
    ..displayDuration = const Duration(milliseconds: 2000)
    ..indicatorType = EasyLoadingIndicatorType.fadingCircle
    ..loadingStyle = EasyLoadingStyle.dark
    ..indicatorSize = 45.0
    ..radius = 10.0
    ..progressColor = Colors.yellow
    ..backgroundColor = Colors.green
    ..indicatorColor = Colors.yellow
    ..textColor = Colors.yellow
    ..maskColor = Colors.blue.withOpacity(0.5)
    ..userInteractions = false
    ..dismissOnTap = false
    ..maskType = EasyLoadingMaskType.black;
}

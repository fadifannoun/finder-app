import 'package:flutter/material.dart';

SnackBar PopNotification(String text) {
  return SnackBar(
    behavior: SnackBarBehavior.floating,
    content: Text(text, textAlign: TextAlign.center),
    shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(20)),
    backgroundColor: Color(0xFF323232).withOpacity(0.75),
    width: 200,
  );
}

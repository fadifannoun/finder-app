import 'dart:convert';

import 'package:findix_app/utils/constants.dart';
import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:findix_app/Components/custom-box-shadow.dart';

class NearbyUser extends StatelessWidget {
  final void Function() onClick;
  final String firstName;
  final String lastName;
  final int distance;
  final String image;
  final String occupation;

  NearbyUser({
    this.onClick,
    this.firstName,
    this.lastName,
    this.distance,
    this.image,
    this.occupation,
  });

  @override
  Widget build(BuildContext context) {
    const String defaultImage = "images/user.png";
    return GestureDetector(
      onTap: () async {
        onClick();
      },
      child: Container(
        margin: EdgeInsets.only(bottom: 20, left: 25, right: 25),
        child: Row(
          children: [
            Container(
              decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(12),
                boxShadow: [
                  CustomBoxShadow(
                    color: primaryBlue.withOpacity(0.4),
                    blurRadius: 15,
                  )
                ],
              ),
              child: ClipRRect(
                borderRadius: BorderRadius.circular(12),
                child: Image(
                  height: 80,
                  width: 80,
                  fit: BoxFit.fill,
                  image: image != null && image != ""
                      ? NetworkImage("$image")
                      : AssetImage(defaultImage),
                  errorBuilder: (BuildContext context, Object exception,
                      StackTrace stackTrace) {
                    return Image.asset(
                      defaultImage,
                      height: 80,
                      width: 80,
                      fit: BoxFit.fill,
                    );
                  },
                ),
              ),
            ),
            SizedBox(width: 7),
            Expanded(
              child: Container(
                //image height
                height: 80,
                padding: EdgeInsets.symmetric(vertical: 15, horizontal: 15),
                decoration: BoxDecoration(
                  color: Colors.white,
                  borderRadius: BorderRadius.all(Radius.circular(12)),
                  boxShadow: [
                    CustomBoxShadow(
                      color: Colors.black.withOpacity(0.14),
                      blurRadius: 15,
                    )
                  ],
                ),
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text(
                      "${firstName ?? 'Guest'} ${lastName ?? ''}",
                      style: TextStyle(
                        fontSize: 16,
                        fontWeight: FontWeight.w600,
                      ),
                    ),
                    Text(
                      occupation ?? "",
                      style: TextStyle(
                        fontSize: 13,
                        fontWeight: FontWeight.w600,
                      ),
                    ),
                    distance != null
                        ? Row(
                            children: [
                              FaIcon(
                                FontAwesomeIcons.mapPin,
                                color: Color(0xFF707070),
                                size: 12,
                              ),
                              SizedBox(width: 5),
                              Text(
                                "${distance}m away from you",
                                style: TextStyle(
                                  fontSize: 12,
                                  fontWeight: FontWeight.w400,
                                  color: Color(0xFF707070),
                                ),
                              ),
                            ],
                          )
                        : Row()
                  ],
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}

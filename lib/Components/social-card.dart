import 'package:findix_app/Components/custom-box-shadow.dart';
import 'package:findix_app/utils/constants.dart';

import 'package:findix_app/utils/storage.dart' as storage;
import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';

class SocialCard extends StatefulWidget {
  final Future<void> Function() callback;
  final IconData icon;
  final String title;
  final String type;

  SocialCard({this.icon, this.title, this.type, this.callback});

  @override
  _SocialCardState createState() => _SocialCardState();
}

class _SocialCardState extends State<SocialCard> {
  String status;
  var _theme = {
    'unfocused': {
      "backgroundColor": Colors.white,
      "textColor": secondaryBlue.withOpacity(0.51),
      "iconColor": Color(0xFF32286D),
    },
    'focused': {
      'backgroundColor': primaryBlue,
      'iconColor': Colors.white,
      'textColor': Colors.white,
    }
  };
  dynamic theme;
  Future<dynamic> getStatus() async {
    status = await storage.read('${widget.title.toLowerCase()}Status');

    setState(() {
      if (status != null && widget.type == "register") {
        theme = _theme['focused'];
      } else {
        theme = _theme['unfocused'];
      }
    });
  }

  @override
  void initState() {
    theme = _theme['unfocused'];
    getStatus();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.all(11),
      child: Container(
        decoration: BoxDecoration(
          color: theme['backgroundColor'],
          borderRadius: BorderRadius.all(Radius.circular(12)),
          boxShadow: [
            CustomBoxShadow(
              color: Colors.black.withOpacity(0.14),
              blurRadius: 15,
            )
          ],
        ),
        child: Material(
          color: Colors.transparent,
          child: InkWell(
            borderRadius: BorderRadius.circular(15),
            highlightColor: Colors.transparent,
            splashColor: Colors.grey.withOpacity(0.1),
            onHighlightChanged: (value) {
              setState(() {
                if (value == true) {
                  theme = _theme['focused'];
                } else if (status == null) {
                  theme = _theme['unfocused'];
                }
              });
            },
            onTap: () async {
              print('tapped');
              await widget.callback();
              await getStatus();
            },
            child: Container(
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  FaIcon(
                    widget.icon,
                    color: theme['iconColor'],
                    size: 40,
                  ),
                  SizedBox(height: 15),
                  Text(
                    widget.title,
                    style: TextStyle(
                      color: theme['textColor'],
                      fontSize: 13,
                    ),
                  )
                ],
              ),
            ),
          ),
        ),
      ),
    );
  }

  // void getInstagramData() async {
  //   FlutterInsta flutterInsta = FlutterInsta();
  //   //get user username
  //   await flutterInsta.getProfileData("username");
  //   print("Username : ${flutterInsta.username}");
  //   print("Followers : ${flutterInsta.followers}");
  //   print("Folowing : ${flutterInsta.following}");
  //   print("Bio : ${flutterInsta.bio}");
  //   print("Website : ${flutterInsta.website}");
  //   print("Profile Image : ${flutterInsta.imgurl}");
  //   print("Feed images:${flutterInsta.feedImagesUrl}");
  // }
}

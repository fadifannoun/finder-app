import 'dart:ui';

import 'package:findix_app/Components/pop-notification.dart';
import 'package:findix_app/services/analytic_service.dart';
import 'package:flutter/material.dart';
import 'package:findix_app/Components/custom-box-shadow.dart';

import 'package:flutter_easyloading/flutter_easyloading.dart';
import 'package:pin_code_fields/pin_code_fields.dart';
import 'package:http/http.dart' as http;
import 'package:findix_app/utils/storage.dart' as storage;
import 'dart:convert';
import 'package:findix_app/utils/constants.dart';
import 'package:findix_app/utils/keys.dart' as storageKeys;

import '../home-page.dart';
import 'package:findix_app/locator.dart';

class VerificationPage extends StatefulWidget {
  final mobileNumber;
  final verificationId;
  VerificationPage({this.mobileNumber, this.verificationId});

  @override
  _VerificationPageState createState() => _VerificationPageState();
}

class _VerificationPageState extends State<VerificationPage> {
  String verificationCode = "";
  bool isRunning;
  final AnalyticsService _analytics = locator<AnalyticsService>();
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      extendBody: true,
      backgroundColor: primaryBlue,
      appBar: AppBar(
        leadingWidth: 30,
        title: Text(
          "Back to edit number",
          style: TextStyle(
            fontSize: 16,
            fontWeight: FontWeight.w400,
          ),
        ),
        elevation: 0,
        backgroundColor: Colors.white.withOpacity(0.07),
      ),
      body: Stack(children: [
        Positioned(
          left: 0,
          right: 0,
          top: MediaQuery.of(context).size.height - 480,
          child: Image(
            image: AssetImage("images/Social media-bro.png"),
          ),
        ),
        Container(
          margin: EdgeInsets.all(20),
          child: Column(
            children: [
              Container(
                margin: EdgeInsets.symmetric(horizontal: 45),
                child: Text(
                  "Please confirm your phone number by entering the code you've received via SMS",
                  textAlign: TextAlign.center,
                  style: TextStyle(
                    fontWeight: FontWeight.w300,
                    fontSize: 15,
                    color: Colors.white,
                  ),
                ),
              ),
              SizedBox(height: 95),
              Container(
                margin: EdgeInsets.symmetric(horizontal: 50),
                child: PinCodeTextField(
                  backgroundColor: Colors.transparent,
                  boxShadows: [
                    CustomBoxShadow(
                      color: Colors.black.withOpacity(0.16),
                      blurRadius: 6,
                      blurStyle: BlurStyle.outer,
                    )
                  ],
                  appContext: context,
                  length: 4,
                  onChanged: (value) {
                    print('changed');
                    setState(() {
                      verificationCode = value;
                    });
                  },
                  keyboardType: TextInputType.number,
                  enableActiveFill: true,
                  cursorColor: primaryBlue,
                  pinTheme: PinTheme(
                    activeColor: Colors.white,
                    activeFillColor: Colors.white,
                    inactiveColor: Colors.white,
                    inactiveFillColor: Colors.white,
                    selectedColor: Colors.white,
                    selectedFillColor: Colors.white,
                    shape: PinCodeFieldShape.box,
                    borderRadius: BorderRadius.circular(10),
                    fieldHeight: 45,
                    fieldWidth: 45,
                  ),
                ),
              ),
              SizedBox(height: 0),
              Builder(
                builder: (context) => ButtonTheme(
                  minWidth: 150,
                  child: RaisedButton(
                    child: Text(
                      'Confirm',
                      style: TextStyle(color: Colors.white),
                    ),
                    onPressed: () async {
                      print(verificationCode);

                      if (verificationCode.length < 4) {
                        print('fill');
                        Scaffold.of(context)
                            .showSnackBar(PopNotification("Invalid Code"));
                      } else {
                        try {
                          EasyLoading.show();
                          var params = {
                            "code": verificationCode,
                            "mobileNumber": widget.mobileNumber,
                          };

                          var response = await http.post("$baseUrl/auth/login",
                              body: params);
                          var res = jsonDecode(response.body);

                          if (res['success'] == true) {
                            var user = res['data']['user'];
                            await storage.store(
                                storageKeys.mobileNumber, widget.mobileNumber);

                            await storage.store(
                                storageKeys.userId, user['_id']);

                            await storage.store(
                                storageKeys.firstName, user['firstName']);
                            await storage.store(
                                storageKeys.lastName, user['lastName'] ?? "");
                            await storage.store(storageKeys.isLoggedIn, "true");
                            await storage.store(storageKeys.showMobileNumber,
                                user['showMobileNumber'].toString() ?? "true");
                            await storage.store(
                                storageKeys.profileImage,
                                user['profileImage'] != null
                                    ? user["profileImage"]
                                    : "");
                            await storage.store(
                                storageKeys.coverImage,
                                user["coverImage"] != null
                                    ? user["coverImage"]
                                    : "");
                            await storage.store(
                                storageKeys.email, user["email"]);
                            await storage.store(
                                storageKeys.website, user["website"]);
                            await storage.store(
                                storageKeys.occupation, user["occupation"]);
                            for (var key in user.keys) {
                              if (key.substring(key.length - 2) == "Id") {
                                await storage.store(key, user[key]);
                                await storage.store(
                                    "${key.substring(0, key.length - 2)}Status",
                                    "connected");
                              }
                            }
                            Navigator.of(context).pushAndRemoveUntil(
                                MaterialPageRoute(
                                  builder: (context) => HomePage(),
                                  settings: RouteSettings(
                                    name: "HomePage",
                                  ),
                                ),
                                (Route<dynamic> route) => false);
                            await _analytics.logLogin();
                          } else {
                            Scaffold.of(context)
                                .showSnackBar(PopNotification(res['message']));
                          }
                          EasyLoading.dismiss();
                        } catch (e) {
                          if (EasyLoading.isShow) await EasyLoading.dismiss();
                          Scaffold.of(context).showSnackBar(
                              PopNotification("Something went wrong"));
                          print(e);
                        }
                      }
                    },
                    color: Color(0xFF29CAE4),
                    shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(20),
                    ),
                  ),
                ),
              )
            ],
          ),
        ),
      ]),
    );
  }
}

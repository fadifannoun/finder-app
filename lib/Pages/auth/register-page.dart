import 'dart:convert';
import 'dart:ui';
import 'package:findix_app/Components/pop-notification.dart';
import 'package:flutter/material.dart';
import 'package:findix_app/Components/custom-box-shadow.dart';
import 'package:findix_app/Pages/auth/verification-page.dart';
import 'package:flutter_easyloading/flutter_easyloading.dart';
import 'package:http/http.dart' as http;
import 'package:findix_app/Pages/home-page.dart';
import 'package:findix_app/utils/constants.dart';

class RegisterPage extends StatefulWidget {
  @override
  _RegisterPageState createState() => _RegisterPageState();
}

class _RegisterPageState extends State<RegisterPage> {
  String countryCode = "";
  String mobileNumber = "";

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      extendBody: true,
      backgroundColor: primaryBlue,
      appBar: AppBar(
        title: Text(
          "Join Ferfax With Your Phone Number",
          style: TextStyle(
            fontSize: 16,
            fontWeight: FontWeight.w400,
          ),
        ),
        centerTitle: true,
        elevation: 0,
        backgroundColor: Colors.white.withOpacity(0.07),
      ),
      body: Stack(children: [
        Positioned(
          left: 0,
          right: 0,
          top: MediaQuery.of(context).size.height - 480,
          child: Image(
            image: AssetImage("images/Social media-bro.png"),
          ),
        ),
        Container(
          margin: EdgeInsets.all(20),
          child: Column(
            children: [
              Container(
                margin: EdgeInsets.symmetric(horizontal: 45),
                child: Text(
                  'Please confirm your country code and enter your phone number',
                  textAlign: TextAlign.center,
                  style: TextStyle(
                    fontWeight: FontWeight.w300,
                    fontSize: 15,
                    color: Colors.white,
                  ),
                ),
              ),
              SizedBox(height: 95),
              Row(
                children: [
                  Expanded(
                    flex: 1,
                    child: Container(
                      decoration: BoxDecoration(
                        color: Colors.white,
                        borderRadius: BorderRadius.all(Radius.circular(10)),
                        boxShadow: [
                          CustomBoxShadow(
                            color: Colors.black.withOpacity(0.16),
                            blurRadius: 6,
                          )
                        ],
                      ),
                      child: TextFormField(
                        textAlign: TextAlign.center,
                        keyboardType: TextInputType.number,
                        maxLength: 3,
                        maxLengthEnforced: true,
                        onChanged: (value) {
                          setState(() {
                            countryCode = value;
                          });
                        },
                        decoration: InputDecoration(
                          prefixIcon: Padding(
                            padding: const EdgeInsets.only(left: 10),
                            child: Text("+",
                                style: TextStyle(color: Colors.black)),
                          ),
                          prefixIconConstraints: BoxConstraints(),
                          counterText: "",
                          contentPadding: EdgeInsets.only(right: 10),
                          hintText: "Code",
                          border: InputBorder.none,
                        ),
                      ),
                    ),
                  ),
                  SizedBox(width: 10),
                  Expanded(
                    flex: 3,
                    child: Container(
                      decoration: BoxDecoration(
                        color: Colors.white,
                        borderRadius: BorderRadius.all(Radius.circular(10)),
                        boxShadow: [
                          CustomBoxShadow(
                            color: Colors.black.withOpacity(0.16),
                            blurRadius: 6,
                          )
                        ],
                      ),
                      child: TextFormField(
                        keyboardType: TextInputType.number,
                        maxLength: 10,
                        maxLengthEnforced: true,
                        onChanged: (value) {
                          setState(() {
                            mobileNumber = value;
                          });
                        },
                        decoration: InputDecoration(
                          counterText: "",
                          contentPadding:
                              EdgeInsets.symmetric(vertical: 0, horizontal: 10),
                          hintText: "Mobile Number",
                          border: InputBorder.none,
                        ),
                      ),
                    ),
                  ),
                ],
              ),
            ],
          ),
        ),
      ]),
      bottomNavigationBar: BottomAppBar(
        color: Colors.transparent,
        elevation: 0,
        child: Container(
          margin: EdgeInsets.symmetric(horizontal: 20),
          height: 60,
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              FlatButton(
                child: Text(
                  "Skip, enter as a guest",
                  style: TextStyle(color: Colors.white),
                ),
                onPressed: () async {
                  if (Navigator.canPop(context)) {
                    Navigator.of(context).pop();
                  } else {
                    Navigator.of(context).pushReplacement(MaterialPageRoute(
                      builder: (context) => HomePage(),
                      settings: RouteSettings(
                        name: "HomePage",
                      ),
                    ));
                  }
                },
              ),
              Builder(
                builder: (context) => ButtonTheme(
                  minWidth: 150,
                  child: RaisedButton(
                    child: Text(
                      "Next",
                      style: TextStyle(color: Colors.white),
                    ),
                    onPressed: () async {
                      if (countryCode.length == 0 || mobileNumber.length == 0) {
                        Scaffold.of(context).showSnackBar(
                            PopNotification("Invalid Phone Number"));
                      } else {
                        await EasyLoading.show();
                        try {
                          var params = {
                            "mobileNumber": "+$countryCode$mobileNumber"
                          };
                          var res = await http.post(
                              "$baseUrl/auth/verification-code",
                              body: params);
                          var data = jsonDecode(res.body);
                          await EasyLoading.dismiss();

                          if (data['success'] == true) {
                            await Navigator.push(
                              context,
                              MaterialPageRoute(
                                builder: (context) => VerificationPage(
                                  mobileNumber: "+$countryCode$mobileNumber",
                                  verificationId: "",
                                ),
                                settings: RouteSettings(
                                  name: "VerificationPage",
                                ),
                              ),
                            );
                          } else {
                            Scaffold.of(context).showSnackBar(
                                PopNotification("Something went wrong"));
                          }
                        } catch (e) {
                          if (EasyLoading.isShow) await EasyLoading.dismiss();
                          Scaffold.of(context).showSnackBar(
                              PopNotification("Something went wrong"));
                          print("error $e");
                        }
                      }
                    },
                    color: Color(0xFF29CAE4),
                    shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(20),
                    ),
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}

import 'package:flutter/material.dart';
import 'personal-data.dart';
import 'social-data.dart';
import 'favorite-users.dart';

class MyProfilePage extends StatefulWidget {
  final void Function(String name) onGetName;
  MyProfilePage({this.onGetName});

  @override
  _MyProfilePageState createState() => _MyProfilePageState();
}

class _MyProfilePageState extends State<MyProfilePage>
    with SingleTickerProviderStateMixin {
  String mobileNumber;

  List<Widget> loggedLabels = [
    Container(
      padding: EdgeInsets.symmetric(horizontal: 20, vertical: 10),
      child: Text('Personal'),
    ),
    Container(
      padding: EdgeInsets.symmetric(horizontal: 20, vertical: 10),
      child: Text('Social'),
    ),
    Container(
      padding: EdgeInsets.symmetric(horizontal: 20, vertical: 10),
      child: Text('Favorites'),
    ),
  ];
  List<Widget> loggedTabs = [
    PersonalData(),
    SocialData(),
    FavoriteUsers(),
  ];

  @override
  void initState() {
    print('profile init');
    super.initState();
  }

  @override
  void dispose() {
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return DefaultTabController(
      length: loggedTabs.length,
      child: Scaffold(
        backgroundColor: Colors.transparent,
        appBar: PreferredSize(
          preferredSize: Size.fromHeight(60),
          child: AppBar(
            backgroundColor: Colors.transparent,
            toolbarHeight: 38,
            elevation: 0,
            bottom: PreferredSize(
              child: Container(
                padding: EdgeInsets.symmetric(horizontal: 25, vertical: 10),
                child: TabBar(
                  labelColor: Color(0xFF231965),
                  unselectedLabelColor: Color(0xFFBEBEBE),
                  labelPadding: EdgeInsets.all(0),
                  indicatorColor: Color(0xFF29206A),
                  indicatorSize: TabBarIndicatorSize.label,
                  tabs: loggedLabels,
                ),
              ),
            ),
          ),
        ),
        body: TabBarView(
          physics: NeverScrollableScrollPhysics(),
          children: [
            PersonalData(onGetName: (String name) {
              widget.onGetName(name);
            }),
            SocialData(onGetName: (String name) {
              widget.onGetName(name);
            }),
            FavoriteUsers(),
          ],
        ),
      ),
    );
  }
}

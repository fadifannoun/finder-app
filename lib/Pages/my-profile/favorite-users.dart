import 'package:flutter/material.dart';
import 'package:findix_app/Components/nearby-user.dart';
import 'package:findix_app/utils/storage.dart' as storage;
import 'package:http/http.dart' as http;
import 'dart:convert';
import 'package:findix_app/Pages/profile-page.dart';
import 'package:findix_app/utils/constants.dart';
import 'package:findix_app/utils/keys.dart' as storageKeys;

class FavoriteUsers extends StatefulWidget {
  @override
  _FavoriteUsersState createState() => _FavoriteUsersState();
}

class _FavoriteUsersState extends State<FavoriteUsers> {
  List<NearbyUser> favoriteUsers = [];
  int followers = 0;
  bool isLoading = false;

  void getFavoriteUsers() async {
    var mobileNumber = await storage.read(storageKeys.mobileNumber);
    var params = {
      "mobileNumber": mobileNumber,
    };
    print(params);
    setState(() {
      isLoading = true;
    });

    var response = await http.post("$baseUrl/user/favorites", body: params);
    var data = jsonDecode(response.body)['data'];
    var users = data["users"];
    int followers = data['followers'];
    setState(() {
      this.followers = followers;
    });

    setState(() {
      users.asMap().forEach((i, user) {
        favoriteUsers.add(NearbyUser(
          firstName: user["firstName"],
          lastName: user["lastName"],
          image: user["profileImage"],
          occupation: user['occupation'],
          onClick: () async {
            await Navigator.push(
              context,
              MaterialPageRoute(
                settings: RouteSettings(
                  name: "FavoriteUserPage",
                ),
                builder: (context) => ProfilePage(
                  id: user['_id'],
                  firstName: user['firstName'],
                  lastName: user['lastName'],
                  coverImage: user['coverImage'],
                  facebookId: user['facebookId'],
                  twitterId: user['twitterId'],
                  youtubeId: user['youtubeId'],
                  githubId: user['githubId'],
                  behanceId: user['behanceId'],
                  linkedinId: user['linkedinId'],
                  snapchatId: user['snapchatId'],
                  tiktokId: user['tiktokId'],
                  instagramId: user['instagramId'],
                  whatsAppId: user['whatsAppId'],
                  pinterestId: user['pinterestId'],
                  twitchId: user['twitchId'],
                  tinderId: user['tinderId'],
                  weChatId: user['weChatId'],
                  soundCloudId: user['soundCloudId'],
                  spotifyId: user['spotifyId'],
                  telegramId: user['telegramId'],
                  email: user['email'],
                  website: user['website'],
                  mobileNumber: user['mobileNumber'],
                  isFavorite: true,
                  followers: user['followers'],
                  occupation: user['occupation'],
                  onPop: (fav) {
                    if (fav == false) {
                      setState(() {
                        favoriteUsers.removeAt(i);
                      });
                    }
                  },
                  //todo:fill all fields
                ),
              ),
            );
          },
        ));
      });
    });
    setState(() {
      isLoading = false;
    });
  }

  @override
  void initState() {
    super.initState();
    getFavoriteUsers();
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      child: isLoading
          ? Center(
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  CircularProgressIndicator(
                    valueColor: AlwaysStoppedAnimation<Color>(primaryBlue),
                  ),
                  SizedBox(height: 10),
                  Text(
                    "Loading...",
                    style: TextStyle(
                      fontSize: 16,
                      fontWeight: FontWeight.w400,
                      color: secondaryBlue.withOpacity(0.2),
                    ),
                  ),
                ],
              ),
            )
          : Column(
              children: [
                Text('$followers people like you'),
                favoriteUsers.length > 0
                    ? Column(children: [
                        SizedBox(height: 20),
                        ...favoriteUsers,
                      ])
                    : Center(
                        child: Text(
                          'Your favorite list is empty\nTry to add some friends.',
                          textAlign: TextAlign.center,
                          style: TextStyle(
                              color: Theme.of(context)
                                  .textTheme
                                  .bodyText1
                                  .color
                                  .withOpacity(0.2)),
                        ),
                      ),
              ],
            ),
    );
  }
}

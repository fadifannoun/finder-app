import 'package:findix_app/services/analytic_service.dart';
import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import 'package:flutter_easyloading/flutter_easyloading.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:findix_app/Components/social-card.dart';
import 'package:findix_app/utils/api.dart';
import 'package:http/http.dart' as http;
import 'package:findix_app/utils/storage.dart' as storage;
import 'package:findix_app/utils/constants.dart';
import 'package:findix_app/utils/keys.dart' as storageKeys;
import 'package:url_launcher/url_launcher.dart';
import 'package:findix_app/locator.dart';

final api = API();

class SocialData extends StatefulWidget {
  final void Function(String name) onGetName;
  SocialData({this.onGetName});

  @override
  _SocialDataState createState() => _SocialDataState();
}

class _SocialDataState extends State<SocialData> {
  final AnalyticsService _analytics = locator<AnalyticsService>();
  @override
  Widget build(BuildContext context) {
    return SingleChildScrollView(
      physics: AlwaysScrollableScrollPhysics(),
      child: Container(
        padding: EdgeInsets.symmetric(horizontal: 20, vertical: 25),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Text(
              'Available Social Accounts',
              style: TextStyle(
                fontSize: 16,
                fontWeight: FontWeight.w600,
                color: Color(0xFF362D70),
              ),
            ),
            SizedBox(height: 5),
            Text(
              'Select a social account to connect',
              style: TextStyle(
                fontSize: 13,
                fontWeight: FontWeight.w400,
                color: secondaryBlue.withOpacity(0.50),
              ),
            ),
            SizedBox(height: 15),
            GridView.count(
              shrinkWrap: true,
              clipBehavior: Clip.none,
              physics: NeverScrollableScrollPhysics(),
              crossAxisCount: 3,
              children: [
                SocialCard(
                  title: 'Facebook',
                  icon: FontAwesomeIcons.facebookF,
                  type: 'register',
                  callback: () async {
                    var id = await storage.read(storageKeys.facebookId);
                    await showModal(
                      id ?? "",
                      "Facebook",
                      FontAwesomeIcons.facebookF,
                      api.loginWithFacebook,
                      steps: [
                        Text("1. Open Messenger App"),
                        Text("2. Press on profile pic on the top left corner"),
                        Text(
                            "3. You will find your username under Username field(after m.me/)"),
                      ],
                      hint: "m.me/username",
                      prefix: 'm.me/',
                    );
                  },
                ),
                SocialCard(
                    title: 'Instagram',
                    icon: FontAwesomeIcons.instagram,
                    type: 'register',
                    callback: () async {
                      var id = await storage.read(storageKeys.instagramId);
                      await showModal(
                        id ?? "",
                        "Instagram",
                        FontAwesomeIcons.instagram,
                        api.loginWithInstagram,
                        steps: [
                          Text("1. Open Instagram App"),
                          Text(
                              "2. Press on profile pic on the bottom right corner"),
                          Text(
                              "3. You will find your username on the top of the screen"),
                        ],
                        hint: "username",
                      );
                    }),
                SocialCard(
                  title: 'YouTube',
                  icon: FontAwesomeIcons.youtube,
                  type: 'register',
                  callback: () async {
                    var id = await storage.read(storageKeys.youtubeId);
                    await showModal(
                      id ?? "",
                      "Youtube",
                      FontAwesomeIcons.youtube,
                      api.loginWithYoutube,
                      steps: [
                        RichText(
                          text: TextSpan(
                            children: [
                              TextSpan(
                                text: "1. Go to ",
                                style: TextStyle(
                                  color: secondaryBlue,
                                  fontSize: 16,
                                ),
                              ),
                              TextSpan(
                                text: "youtube.com/account_advanced",
                                style: TextStyle(
                                  color: Colors.blueAccent,
                                  fontSize: 16,
                                  decoration: TextDecoration.underline,
                                ),
                                recognizer: TapGestureRecognizer()
                                  ..onTap = () {
                                    launch(
                                        "https://youtube.com/account_advanced");
                                  },
                              )
                            ],
                          ),
                        ),
                        Text(
                            "2. You will find your username under User ID field"),
                      ],
                      hint: "userId",
                    );
                  },
                ),
                SocialCard(
                  title: 'TikTok',
                  icon: FontAwesomeIcons.tiktok,
                  type: 'register',
                  callback: () async {
                    var id = await storage.read(storageKeys.tiktokId);
                    await showModal(
                      id ?? "",
                      "TikTok",
                      FontAwesomeIcons.tiktok,
                      api.loginWithTiktok,
                      steps: [
                        Text("1. Open TikTok App"),
                        Text("2. Press on 'Me' on the bottom right corner"),
                        Text("3. You will find your username after @"),
                      ],
                      hint: "@username",
                      prefix: "@",
                    );
                  },
                ),
                SocialCard(
                  title: 'LinkedIn',
                  icon: FontAwesomeIcons.linkedinIn,
                  type: 'register',
                  callback: () async {
                    var id = await storage.read(storageKeys.linkedinId);
                    await showModal(id ?? "", "LinkedIn",
                        FontAwesomeIcons.linkedinIn, api.loginWithLinkedin,
                        steps: [
                          Text("1. Open LinkedIn App"),
                          Text(
                              "2. Press on profile pic on the top left corner"),
                          Text("3. Press on View Profile"),
                          Text("4. Scroll Down to the Contact Section"),
                          Text(
                              "5. You will find your username under Your Profile field (after /in/)"),
                        ],
                        hint: "in/username",
                        prefix: 'in/');
                  },
                ),
                SocialCard(
                  title: 'Twitter',
                  icon: FontAwesomeIcons.twitter,
                  type: 'register',
                  callback: () async {
                    var id = await storage.read(storageKeys.twitterId);
                    await showModal(id ?? "", "Twitter",
                        FontAwesomeIcons.twitter, api.loginWithTwitter,
                        steps: [
                          Text("1. Open Twitter App"),
                          Text("2. Open the side menu on the left"),
                          Text("3. You will find your username after @"),
                        ],
                        hint: "@username",
                        prefix: "@");
                  },
                ),
                SocialCard(
                  title: 'Snapchat',
                  icon: FontAwesomeIcons.snapchatGhost,
                  type: 'register',
                  callback: () async {
                    var id = await storage.read(storageKeys.snapchatId);
                    await showModal(
                      id ?? "",
                      "Snapchat",
                      FontAwesomeIcons.snapchatGhost,
                      api.loginWithSnapchat,
                      steps: [
                        Text("1. Open Snapchat App"),
                        Text("2. Press on your avatar on the top left corner"),
                        Text(
                            "3. You will find your username under your snapcode and display name"),
                      ],
                      hint: 'username',
                    );
                  },
                ),
                SocialCard(
                  title: 'Behance',
                  icon: FontAwesomeIcons.behance,
                  type: 'register',
                  callback: () async {
                    var id = await storage.read(storageKeys.behanceId);
                    await showModal(
                      id ?? "",
                      "Behance",
                      FontAwesomeIcons.behance,
                      api.loginWithBehance,
                      steps: [
                        RichText(
                          text: TextSpan(
                            children: [
                              TextSpan(
                                text: "1. Go to ",
                                style: TextStyle(
                                  color: secondaryBlue,
                                  fontSize: 16,
                                ),
                              ),
                              TextSpan(
                                text: "behance.net/account/settings",
                                style: TextStyle(
                                  color: Colors.blueAccent,
                                  fontSize: 16,
                                  decoration: TextDecoration.underline,
                                ),
                                recognizer: TapGestureRecognizer()
                                  ..onTap = () {
                                    launch(
                                        "https://behance.net/account/settings");
                                  },
                              ),
                            ],
                          ),
                        ),
                        Text(
                            "2. You will find your username under Behance URL (after https://www.behance.net/)"),
                      ],
                      hint: 'https://www.behance.net/username',
                      prefix: "https://www.behance.net/",
                    );
                  },
                ),
                SocialCard(
                  title: 'Github',
                  icon: FontAwesomeIcons.github,
                  type: 'register',
                  callback: () async {
                    var id = await storage.read(storageKeys.githubId);
                    await showModal(
                      id ?? "",
                      "Github",
                      FontAwesomeIcons.github,
                      api.loginWithGithub,
                      steps: [
                        Text("1. Open Github App"),
                        Text("2. Go to Profile tab"),
                        Text(
                            "3. You will find your username on the top of the screen, under your display name")
                      ],
                      hint: 'username',
                    );
                  },
                ),
                SocialCard(
                  title: 'WhatsApp',
                  icon: FontAwesomeIcons.whatsapp,
                  type: 'register',
                  callback: () async {
                    var id = await storage.read(storageKeys.whatsAppId);
                    await showModal(id ?? "", "WhatsApp",
                        FontAwesomeIcons.whatsapp, api.loginWithWhatsApp(),
                        steps: [
                          Text(
                              "Enter your full whatsapp number without + or 00")
                        ],
                        hint: 'number',
                        pattern: r'(?<=(\+|00)).*');
                  },
                ),
                SocialCard(
                  title: 'Pinterest',
                  icon: FontAwesomeIcons.pinterestP,
                  type: 'register',
                  callback: () async {
                    var id = await storage.read(storageKeys.pinterestId);
                    await showModal(
                      id ?? "",
                      "Pinterest",
                      FontAwesomeIcons.pinterestP,
                      api.loginWithPinterest(),
                      steps: [
                        Text("1. Open Pinterest App"),
                        Text("2. Tap on your profile pic on the bottom tray"),
                        Text(
                            "3. Tap again on your profile pic on the top left corner"),
                        Text("4. Tap on the settings icon on the top right"),
                        Text("5. Go to 'Edit profile' section"),
                        Text("6. Search for username under 'username' section"),
                      ],
                      hint: 'username',
                    );
                  },
                ),
                SocialCard(
                  title: 'Twitch',
                  icon: FontAwesomeIcons.twitch,
                  type: 'register',
                  callback: () async {
                    var id = await storage.read(storageKeys.twitchId);
                    await showModal(
                      id ?? "",
                      "Twitch",
                      FontAwesomeIcons.twitch,
                      api.loginWithTwitch(),
                      steps: [
                        Text("1. Open Twitch app"),
                        Text(
                            "2. Tap on your profile pic on the top left corner"),
                        Text(
                            "3. Search for your username under your profile picture"),
                      ],
                      hint: 'username',
                    );
                  },
                ),
                SocialCard(
                  title: 'Tinder',
                  icon: FontAwesomeIcons.fireAlt,
                  type: 'register',
                  callback: () async {
                    var id = await storage.read(storageKeys.tinderId);
                    await showModal(
                      id ?? "",
                      "Tinder",
                      FontAwesomeIcons.fireAlt,
                      api.loginWithTinder(),
                      steps: [],
                      hint: 'username',
                    );
                  },
                ),
                SocialCard(
                  title: 'WeChat',
                  icon: FontAwesomeIcons.weixin,
                  type: 'register',
                  callback: () async {
                    var id = await storage.read(storageKeys.weChatId);
                    await showModal(
                      id ?? "",
                      "WeChat",
                      FontAwesomeIcons.weixin,
                      api.loginWithWeChat(),
                      steps: [
                        Text("1. Open WeChat App"),
                        Text("2. Tap on your profile on the bottom right"),
                        Text("3. Tap again on your name on the top"),
                        Text("4. Search for your id next to WeChat ID"),
                      ],
                      hint: 'WeChat ID',
                    );
                  },
                ),
                SocialCard(
                  title: 'SoundCloud',
                  icon: FontAwesomeIcons.soundcloud,
                  type: 'register',
                  callback: () async {
                    var id = await storage.read(storageKeys.soundCloudId);
                    await showModal(
                      id ?? "",
                      "SoundCloud",
                      FontAwesomeIcons.soundcloud,
                      api.loginWithSoundCloud(),
                      steps: [],
                      hint: 'player',
                    );
                  },
                ),
                SocialCard(
                  title: 'Spotify',
                  icon: FontAwesomeIcons.spotify,
                  type: 'register',
                  callback: () async {
                    var id = await storage.read(storageKeys.spotifyId);
                    await showModal(
                      id ?? "",
                      "Spotify",
                      FontAwesomeIcons.spotify,
                      api.loginWithSpotify(),
                      steps: [
                        Text("1. Open Spotify App"),
                        Text("2. Tap on settings icon on the top right"),
                        Text("3. Tap on View Profile"),
                        Text("4. Tap on the three dots on the top right"),
                        Text("5. Tap on Share"),
                        Text("6. Tap on Copy Link"),
                        Text("7. Paste the link in the username field"),
                        Text(
                            "8. The app will clean the link and keep the username"),
                      ],
                      hint: 'username',
                      pattern: r'(?<=https://open.spotify.com/user/)(.*)(?=\?)',
                    );
                  },
                ),
                SocialCard(
                  title: 'Telegram',
                  icon: FontAwesomeIcons.telegramPlane,
                  type: 'register',
                  callback: () async {
                    var id = await storage.read(storageKeys.telegramId);
                    await showModal(id ?? "", "Telegram",
                        FontAwesomeIcons.telegramPlane, api.loginWithTelegram(),
                        steps: [
                          Text("1. Open Telegram app"),
                          Text("2. Tap on the icon on the top left"),
                          Text("3. Tap on your profile pic"),
                          Text(
                              "4. Look for your username under the Username section"),
                        ],
                        hint: 'username',
                        pattern: r'(?<=@)(.*)');
                  },
                ),
              ],
            )
          ],
        ),
      ),
    );
  }

  Future<void> showModal(value, title, icon, onFetch,
      {steps, String hint, String prefix, String pattern}) {
    TextEditingController _controller = TextEditingController()..text = value;
    var data = {};
    return showDialog<void>(
      barrierDismissible: true,
      context: context,
      barrierColor: primaryBlue.withOpacity(0.75),
      builder: (BuildContext context) {
        return Scaffold(
          backgroundColor: Colors.transparent,
          resizeToAvoidBottomInset: false,
          body: Builder(
            builder: (context) => Theme(
              data: Theme.of(context)
                  .copyWith(iconTheme: IconThemeData(color: Color(0xFF32286D))),
              child: Stack(
                children: [
                  GestureDetector(onTap: () {
                    Navigator.pop(context);
                  }),
                  Container(
                    decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(20),
                      color: Colors.white,
                    ),
                    height: 200,
                    alignment: Alignment.topCenter,
                    margin: EdgeInsets.symmetric(horizontal: 20, vertical: 105),
                    padding: EdgeInsets.symmetric(horizontal: 20, vertical: 15),
                    child: Column(
                      mainAxisSize: MainAxisSize.min,
                      children: [
                        Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: [
                            Container(
                              child: Row(
                                children: [
                                  FaIcon(icon),
                                  SizedBox(width: 8),
                                  Text(title),
                                  IconButton(
                                      padding: EdgeInsets.zero,
                                      icon: Icon(
                                        Icons.info_outline,
                                        color: Color(0xFFAAAAAA),
                                        size: 25,
                                      ),
                                      onPressed: () {
                                        showInfoModal(context, steps: steps);
                                      }),
                                ],
                              ),
                            ),
                            IconButton(
                              icon: FaIcon(
                                FontAwesomeIcons.timesCircle,
                                color: Color(0xFFAAAAAA),
                              ),
                              onPressed: () {
                                Navigator.pop(context);
                              },
                            ),
                          ],
                        ),
                        Row(
                          children: [
                            Expanded(
                              child: TextFormField(
                                controller: _controller,
                                enabled: true,
                                decoration: InputDecoration(
                                  border: OutlineInputBorder(),
                                  hintText: hint ?? "",
                                ),
                              ),
                            ),
                          ],
                        ),
                        SizedBox(height: 15),
                        Row(
                          mainAxisAlignment: MainAxisAlignment.spaceAround,
                          children: [
                            TextButton(
                              child: Text("Fetch Username"),
                              onPressed: () async {
                                try {
                                  EasyLoading.show();

                                  data = await onFetch();
                                } catch (e) {
                                  print(e);
                                } finally {
                                  EasyLoading.dismiss();
                                }
                                print("data: $data");
                                if (data == null || data['username'] == null) {
                                  String title = "Couldn't fetch username";
                                  String body = "Please fill username manually";
                                  showAlertDialog(title, body: body);
                                } else {
                                  _controller.text = data['username'];
                                }
                              },
                            ),
                            TextButton(
                              child: Text("Save"),
                              onPressed: () async {
                                if (_controller.text != "") {
                                  if (prefix != null) {
                                    if (_controller.text.startsWith(prefix)) {
                                      setState(() {
                                        _controller.text = _controller.text
                                            .replaceFirst(prefix, "");
                                      });
                                    }
                                  }
                                  if (pattern != null) {
                                    String s = RegExp(pattern)
                                        .stringMatch(_controller.text);
                                    if (s != null) {
                                      _controller.text = s;
                                    }
                                  }
                                  EasyLoading.show();
                                  if (data == null || data.length == 0) {
                                    data = {};
                                    data['mobileNumber'] = await storage
                                        .read(storageKeys.mobileNumber);
                                    data['social'] = title.toLowerCase();
                                  } else {
                                    var name = await storage
                                        .read(storageKeys.firstName);

                                    if ((name == null || name == "Guest") &&
                                        (data['firstName'] != null)) {
                                      storage.store(storageKeys.firstName,
                                          data['firstName']);
                                      name = data['firstName'];

                                      if (data['lastName'] != null) {}
                                      widget.onGetName(name);
                                    }
                                    var profileImage = await storage
                                        .read(storageKeys.profileImage);
                                    print("profileImage: $profileImage");
                                    if (profileImage == null ||
                                        profileImage == "") {
                                      storage.store(storageKeys.profileImage,
                                          data['image']);
                                      print("image: ${data['image']}");
                                    }
                                  }
                                  data['username'] = _controller.text;
                                  await storage.store(
                                      "${data['social']}Id", data['username']);
                                  await storage.store(
                                      '${data['social']}Status', 'connected');

                                  print(data);
                                  var res = await http.post(
                                      "$baseUrl/user/profile/social/${data['social']}",
                                      body: data);
                                  EasyLoading.dismiss();

                                  //check if response is successful
                                  await _analytics.logSocialConnected(
                                      social: title);

                                  Navigator.pop(context);
                                } else {
                                  showAlertDialog("username cannot be empty");
                                }
                              },
                            )
                          ],
                        )
                      ],
                    ),
                  ),
                ],
              ),
            ),
          ),
        );
      },
    );
  }

  Future<void> showAlertDialog(title, {body}) async {
    return showDialog<void>(
      context: context,
      barrierDismissible: true,
      builder: (BuildContext context) {
        return AlertDialog(
          title: Text('Error!'),
          content: SingleChildScrollView(
            child: ListBody(
              children: <Widget>[
                Text(title),
                SizedBox(height: 10),
                Text(body ?? ""),
              ],
            ),
          ),
          actions: <Widget>[
            TextButton(
              child: Text('Ok'),
              onPressed: () {
                Navigator.of(context).pop();
              },
            ),
          ],
        );
      },
    );
  }
}

showInfoModal(context, {List<Widget> steps}) {
  showDialog(
      context: context,
      builder: (BuildContext context) {
        return Container(
          color: Colors.transparent,
          child: Stack(
            children: [
              GestureDetector(
                onTap: () {
                  Navigator.pop(context);
                },
              ),
              Center(
                child: ClipRRect(
                  borderRadius: BorderRadius.circular(20),
                  child: Container(
                    width: MediaQuery.of(context).size.width - 30,
                    height: MediaQuery.of(context).size.height / 3,
                    padding: EdgeInsets.all(0),
                    decoration: BoxDecoration(
                      shape: BoxShape.rectangle,
                      color: Colors.white,
                      borderRadius: BorderRadius.circular(20),
                    ),
                    child: SingleChildScrollView(
                      child: Container(
                        padding: EdgeInsets.all(20),
                        child: Column(
                          children: [
                            Text(
                              "To enter your username manually, follow these steps: ",
                              style: TextStyle(
                                fontWeight: FontWeight.bold,
                                fontSize: 20,
                              ),
                            ),
                            SizedBox(height: 15),
                            Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: steps,
                            ),
                          ],
                        ),
                      ),
                    ),
                  ),
                ),
              ),
            ],
          ),
        );
      });
}

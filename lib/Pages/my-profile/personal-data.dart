import 'dart:io';

import 'package:findix_app/Components/custom-box-shadow.dart';
import 'package:findix_app/Pages/auth/register-page.dart';
import 'package:flutter/material.dart';
import 'package:flutter_easyloading/flutter_easyloading.dart';
import 'package:image_picker/image_picker.dart';
import 'package:findix_app/utils/storage.dart' as storage;
import 'package:http/http.dart' as http;
import 'package:findix_app/utils/constants.dart';
import 'dart:convert';
import 'package:findix_app/utils/keys.dart' as storageKeys;

import 'package:path/path.dart';
import 'package:shared_preferences/shared_preferences.dart';

class PersonalData extends StatefulWidget {
  final void Function(String name) onGetName;
  PersonalData({this.onGetName});

  @override
  _PersonalDataState createState() => _PersonalDataState();
}

class _PersonalDataState extends State<PersonalData> {
  TextEditingController _nameController;
  TextEditingController _emailController;
  TextEditingController _mobileNumberController;
  TextEditingController _websiteController;
  TextEditingController _occupationController;

  String name, email, mobileNumber, website, occupation;

  ImageSource source;

  // File profileImageFile;
  // File coverImageFile;
  NetworkImage profileImage;
  NetworkImage coverImage;
  bool isDirty = false;
  bool showMobileNumber = true;

  final picker = ImagePicker();

  Future<File> getImage(ImageSource source) async {
    print(source);
    try {
      final pickedFile = await picker.getImage(source: source);
      PickedFile img;
      if (pickedFile != null) {
        img = pickedFile;
        return File(img.path);
      }
      print('No image selected.');
      return null;
    } catch (e) {
      print("error: image is null");
      return null;
    }
  }

  Future<String> uploadImage(File imageFile, String field) async {
    String path = basename(imageFile.path);
    int length = await imageFile.length();
    Uri uri = Uri.parse('$baseUrl/user/profile/personal');
    var stream = new http.ByteStream(imageFile.openRead());
    stream.cast();

    var params = {
      "userId": await storage.read(storageKeys.userId),
    };
    var request = new http.MultipartRequest("POST", uri)
      ..fields["params"] = jsonEncode(params);

    var multipartFile = new http.MultipartFile(
      field,
      stream,
      length,
      filename: path,
    );

    request.files.add(multipartFile);

    var streamedResponse = await request.send();
    if (streamedResponse.statusCode >= 200 &&
        streamedResponse.statusCode < 300) {
      var response = await http.Response.fromStream(streamedResponse);
      try {
        var res = jsonDecode(response.body);
        if (res["success"]) {
          print("success");
          print(res["data"]["user"]);
          print(res["data"]["user"][field]);
          var path = res["data"]["user"][field];
          storage.store(field, path);
          return path;
        } else {
          print("error: ${res["message"]}");
          return null;
        }
      } catch (e) {
        print("error: $e");
        return null;
      }
    } else {
      print("error ${await streamedResponse.stream.bytesToString()}");
    }
  }

  void getData() async {
    _nameController.text = await storage.read(storageKeys.firstName) +
        " " +
        ((await storage.read(storageKeys.lastName)) ?? "");
    _emailController.text = await storage.read(storageKeys.email);
    _mobileNumberController.text = await storage.read(storageKeys.mobileNumber);
    _websiteController.text = await storage.read(storageKeys.website);
    _occupationController.text =
        (await storage.read(storageKeys.occupation)) ?? "";

    bool _showMobileNumber = true;
    String showNumber = await storage.read(storageKeys.showMobileNumber);
    if (showNumber != null) {
      if (showNumber.toLowerCase() == false.toString().toLowerCase()) {
        _showMobileNumber = false;
      } else {
        _showMobileNumber = true;
      }
    } else {
      _showMobileNumber = true;
    }

    var profileImage = await storage.read(storageKeys.profileImage);
    var coverImage = await storage.read(storageKeys.coverImage);

    setState(() {
      name = _nameController.text;
      email = _emailController.text;
      mobileNumber = _mobileNumberController.text;
      website = _websiteController.text;
      showMobileNumber = _showMobileNumber;
      if (profileImage != null && profileImage != "") {
        this.profileImage = NetworkImage("$profileImage");
      }
      if (coverImage != null && coverImage != "") {
        print("cover image not null");

        this.coverImage = NetworkImage("$coverImage");
      }
    });
    print(email);
  }

  @override
  void initState() {
    print("personal data init");
    super.initState();
    _nameController = TextEditingController();
    _emailController = TextEditingController();
    _mobileNumberController = TextEditingController();
    _websiteController = TextEditingController();
    _occupationController = TextEditingController();
    getData();
  }

  @override
  Widget build(BuildContext context) {
    Future<void> showImageDialog() async {
      return showDialog<void>(
        context: context,
        barrierDismissible: true,
        builder: (BuildContext context) {
          return AlertDialog(
            title: Text('Choose a photo'),
            content: SingleChildScrollView(
              child: ListBody(
                children: <Widget>[
                  Text(
                      'Pick a photo either from gallery or take a photo from camera.'),
                ],
              ),
            ),
            actions: <Widget>[
              TextButton(
                child: Text('Choose from gallery'),
                onPressed: () {
                  setState(() {
                    source = ImageSource.gallery;
                  });
                  Navigator.of(context).pop();
                },
              ),
              TextButton(
                child: Text('Take a photo'),
                onPressed: () {
                  setState(() {
                    source = ImageSource.camera;
                  });
                  Navigator.of(context).pop();
                },
              ),
            ],
          );
        },
      );
    }

    return SingleChildScrollView(
      child: Container(
        margin: EdgeInsets.only(top: 25, left: 20, right: 20),
        child: Column(
          children: [
            Row(
              children: [
                ClipRRect(
                    borderRadius: BorderRadius.circular(12),
                    child: GestureDetector(
                        onTap: () async {
                          await showImageDialog();
                          File img = await getImage(source);
                          setState(() {
                            source = null;
                          });
                          if (img != null) {
                            String imgPath =
                                await uploadImage(img, "profileImage");
                            print("imgPath: $imgPath");
                            setState(() {
                              profileImage = NetworkImage("$imgPath");
                            });
                          }
                        },
                        child: Image(
                          image: profileImage != null
                              ? profileImage
                              : AssetImage("images/user.png"),
                          height: 80,
                          width: 80,
                          fit: BoxFit.cover,
                        ))),
                SizedBox(width: 7),
                Expanded(
                  child: ClipRRect(
                    borderRadius: BorderRadius.circular(12),
                    child: GestureDetector(
                      onTap: () async {
                        await showImageDialog();
                        var img = await getImage(source);
                        setState(() {
                          source = null;
                        });
                        if (img != null) {
                          String imgPath = await uploadImage(img, "coverImage");

                          setState(() {
                            coverImage = NetworkImage("$imgPath");
                          });
                        }
                      },
                      child: Image(
                        image: coverImage != null
                            ? coverImage
                            : AssetImage('images/cover.png'),
                        height: 80,
                        // width: 240,
                        fit: BoxFit.cover,
                      ),
                    ),
                  ),
                ),
              ],
            ),
            SizedBox(height: 20),
            Container(
              decoration: BoxDecoration(
                color: Colors.transparent,
                borderRadius: BorderRadius.all(Radius.circular(10)),
                boxShadow: [
                  CustomBoxShadow(
                    color: Colors.black.withOpacity(0.16),
                    blurRadius: 6,
                  )
                ],
              ),
              child: TextFormField(
                controller: _nameController,
                onChanged: (value) {
                  setState(() {
                    name = value;
                    isDirty = true;
                    print(name);
                  });
                },
                decoration: InputDecoration(
                  contentPadding:
                      EdgeInsets.symmetric(vertical: 0, horizontal: 10),
                  hintText: "Name",
                  border: InputBorder.none,
                ),
              ),
            ),
            SizedBox(height: 15),
            Container(
              decoration: BoxDecoration(
                color: Colors.white,
                borderRadius: BorderRadius.all(Radius.circular(10)),
                boxShadow: [
                  CustomBoxShadow(
                    color: Colors.black.withOpacity(0.16),
                    blurRadius: 6,
                  )
                ],
              ),
              child: TextFormField(
                controller: _emailController,
                onChanged: (value) {
                  setState(() {
                    email = value;
                    isDirty = true;
                  });
                },
                decoration: InputDecoration(
                  contentPadding:
                      EdgeInsets.symmetric(vertical: 0, horizontal: 10),
                  hintText: "Email",
                  border: InputBorder.none,
                ),
              ),
            ),
            SizedBox(height: 15),
            Container(
              decoration: BoxDecoration(
                color: Colors.white,
                borderRadius: BorderRadius.all(Radius.circular(10)),
                boxShadow: [
                  CustomBoxShadow(
                    color: Colors.black.withOpacity(0.16),
                    blurRadius: 6,
                  )
                ],
              ),
              child: TextFormField(
                readOnly: true,
                controller: _mobileNumberController,
                onChanged: (value) {
                  setState(() {
                    mobileNumber = value;
                    isDirty = true;
                  });
                },
                decoration: InputDecoration(
                  contentPadding:
                      EdgeInsets.symmetric(vertical: 0, horizontal: 10),
                  hintText: "Mobile Number",
                  border: InputBorder.none,
                ),
              ),
            ),
            SizedBox(height: 15),
            Container(
              decoration: BoxDecoration(
                color: Colors.white,
                borderRadius: BorderRadius.all(Radius.circular(10)),
                boxShadow: [
                  CustomBoxShadow(
                    color: Colors.black.withOpacity(0.16),
                    blurRadius: 6,
                  )
                ],
              ),
              child: TextFormField(
                controller: _websiteController,
                onChanged: (value) {
                  setState(() {
                    website = value;
                    isDirty = true;
                  });
                },
                decoration: InputDecoration(
                  contentPadding:
                      EdgeInsets.symmetric(vertical: 0, horizontal: 10),
                  hintText: "Website",
                  border: InputBorder.none,
                ),
              ),
            ),
            SizedBox(height: 15),
            Container(
              decoration: BoxDecoration(
                color: Colors.white,
                borderRadius: BorderRadius.all(Radius.circular(10)),
                boxShadow: [
                  CustomBoxShadow(
                    color: Colors.black.withOpacity(0.16),
                    blurRadius: 6,
                  )
                ],
              ),
              child: TextFormField(
                controller: _occupationController,
                onChanged: (value) {
                  setState(() {
                    occupation = value;
                    isDirty = true;
                  });
                },
                decoration: InputDecoration(
                  contentPadding:
                      EdgeInsets.symmetric(vertical: 0, horizontal: 10),
                  hintText: "Occupation",
                  border: InputBorder.none,
                ),
              ),
            ),
            SizedBox(height: 15),
            SwitchListTile(
              value: showMobileNumber,
              onChanged: (value) async {
                setState(() {
                  isDirty = true;
                  showMobileNumber = value;
                });
              },
              title: Text("Show Mobile Number to Public"),
            ),
            SizedBox(height: 15),
            ButtonTheme(
              minWidth: 150,
              child: RaisedButton(
                color: Color(0xFF29CAE4),
                shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(20),
                ),
                child: Text(
                  isDirty ? 'Save' : 'Saved!',
                  style: TextStyle(color: Colors.white),
                ),
                elevation: isDirty ? null : 0,
                disabledColor: isDirty ? Colors.grey : Colors.green,
                onPressed: isDirty
                    ? () async {
                        print('clicked');
                        EasyLoading.show();
                        var fullName = name.split(" ");
                        var firstName = "", lastName = "";
                        firstName = fullName[0];
                        if (fullName.length > 1) {
                          lastName = fullName[1];
                        }
                        var user = await storage.read(storageKeys.userId);

                        var params = {
                          'firstName': firstName ?? "",
                          'lastName': lastName ?? "",
                          'email': email ?? "",
                          // 'mobileNumber': mobileNumber,
                          'website': website ?? "",
                          'userId': user,
                          'showMobileNumber': showMobileNumber,
                          'occupation': occupation,
                        };
                        // print(params);

                        var response = await http.post(
                            '$baseUrl/user/profile/personal',
                            body: {"params": jsonEncode(params)});
                        EasyLoading.dismiss();
                        var res = jsonDecode(response.body);
                        print(res);
                        print(res["success"]);
                        if (res["success"]) {
                          if (firstName != "") {
                            widget.onGetName(firstName);
                            storage.store(storageKeys.firstName, firstName);
                          }
                          if (lastName != "") {
                            storage.store(storageKeys.lastName, lastName);
                          }
                          if (email != "") {
                            storage.store(storageKeys.email, email);
                          }
                          // store('mobileNumber', mobileNumber);
                          if (website != "") {
                            storage.store(storageKeys.website, website);
                          }
                          if (occupation != "") {
                            storage.store(storageKeys.occupation, occupation);
                          }
                          storage.store(storageKeys.showMobileNumber,
                              showMobileNumber.toString());
                          ScaffoldMessenger.of(context).showSnackBar(
                            SnackBar(
                              behavior: SnackBarBehavior.floating,
                              // width: 250,
                              shape: RoundedRectangleBorder(
                                borderRadius: BorderRadius.circular(20),
                              ),
                              duration: Duration(seconds: 2),
                              content: Text(
                                "Information Updated Successfully",
                                textAlign: TextAlign.center,
                              ),
                              backgroundColor: Colors.green,
                            ),
                          );
                          setState(() {
                            isDirty = false;
                          });
                        } else {
                          ScaffoldMessenger.of(context).showSnackBar(
                            SnackBar(
                              behavior: SnackBarBehavior.floating,
                              width: 200,
                              shape: RoundedRectangleBorder(
                                borderRadius: BorderRadius.circular(20),
                              ),
                              duration: Duration(seconds: 2),
                              content: Text(
                                "Something went wrong",
                                textAlign: TextAlign.center,
                              ),
                              backgroundColor: Colors.red,
                            ),
                          );
                        }
                      }
                    : null,
              ),
            ),
            ButtonTheme(
              minWidth: 150,
              child: RaisedButton(
                color: Color(0xFF29CAE4),
                shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(20),
                ),
                child: Text(
                  'Logout',
                  style: TextStyle(color: Colors.white),
                ),
                onPressed: () async {
                  EasyLoading.show();
                  await storage.clear();
                  SharedPreferences prefs =
                      await SharedPreferences.getInstance();
                  await prefs.clear();

                  EasyLoading.dismiss();
                  Navigator.of(context).pushAndRemoveUntil(
                    MaterialPageRoute(
                      builder: (context) => RegisterPage(),
                      settings: RouteSettings(
                        name: "RegisterPage",
                      ),
                    ),
                    (_) => false,
                  );
                },
              ),
            ),
          ],
        ),
      ),
    );
  }
}

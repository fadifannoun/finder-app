import 'dart:ui';
import 'package:background_locator/settings/android_settings.dart';
import 'package:background_locator/settings/locator_settings.dart';
import 'package:findix_app/Pages/auth/register-page.dart';
import 'package:findix_app/services/analytic_service.dart';
import 'package:findix_app/locator.dart';
import 'package:findix_app/utils/search-delegate.dart';
import 'package:location_permissions/location_permissions.dart';
import 'package:findix_app/utils/constants.dart';
import 'package:findix_app/utils/location_callback_handler.dart';
import 'package:findix_app/utils/storage.dart' as storage;
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'search-page.dart';
import 'my-profile/my-profile-page.dart';
import 'package:flutter_switch/flutter_switch.dart';
import 'package:http/http.dart' as http;
import 'package:background_locator/background_locator.dart';
import 'package:findix_app/utils/keys.dart' as storageKeys;

class HomePage extends StatefulWidget {
  @override
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> with TickerProviderStateMixin {
  TabController _tabController;
  final tabs = ["Search Tab", "Profile Tab"];
  final AnalyticsService _analytics = locator<AnalyticsService>();
  String userId;
  String name = "";
  String greet = "Hello, Guest";
  bool isVisible = true;

  bool isRunning;

  String isLoggedIn = "false";
  SharedPreferences prefs;

  initData() async {
    prefs = await SharedPreferences.getInstance();
    var id = await storage.read(storageKeys.userId);
    var loggedIn = await storage.read(storageKeys.isLoggedIn);
    bool isVisible = prefs.getBool('isVisible') ?? true;

    var name = await storage.read(storageKeys.firstName);
    name = name ?? "Guest";
    setState(() {
      userId = id;
      isLoggedIn = loggedIn;
      this.isVisible = isVisible;
      this.name = name;
      greet = "Hello, $name";
    });
  }

  @override
  void initState() {
    super.initState();

    _tabController = TabController(length: 2, vsync: this);
    _tabController.addListener(onTap);

    print("home init");
    initData();
    initPlatformState();
  }

  @override
  void dispose() {
    super.dispose();
  }

  Future<void> initPlatformState() async {
    print('Initializing...');
    await BackgroundLocator.initialize();
    print('Initialization done');
    final _isRunning = await BackgroundLocator.isServiceRunning();
    setState(() {
      isRunning = _isRunning;
    });
    print('Running ${isRunning.toString()}');
    _onStart();
  }

  onTap() {
    _analytics.logTabChanged(tab: tabs[_tabController.index]);
    if (_tabController.index == 1 && isLoggedIn != "true") {
      setState(() {
        _tabController.index = 0;
      });
      Navigator.push(
          context,
          MaterialPageRoute(
            builder: (context) => RegisterPage(),
            settings: RouteSettings(
              name: "RegisterPage",
            ),
          ));
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('$greet'),
        backgroundColor: primaryBlue,
        elevation: 0,
        leading: IconButton(
          icon: Icon(
            Icons.search,
            color: Colors.white,
          ),
          onPressed: () async {
            await showSearch(
              context: context,
              delegate: CustomSearchDelegate(contextPage: context),
            );
            print("search");
          },
        ),
        actions: [
          Center(
            child: Padding(
              padding: EdgeInsets.symmetric(horizontal: 20),
              child: FlutterSwitch(
                value: isVisible,
                onToggle: (value) async {
                  String status = value ? "online" : "offline";
                  ScaffoldMessenger.of(context).showSnackBar(
                    SnackBar(
                      backgroundColor: value ? Colors.green : Colors.red,
                      // elevation: 0,
                      // behavior: SnackBarBehavior.floating,
                      // width: 150,
                      // shape: RoundedRectangleBorder(
                      //   borderRadius: BorderRadius.circular(20),
                      // ),
                      duration: Duration(seconds: 2),
                      content: Text(
                        "You are $status",
                        textAlign: TextAlign.center,
                      ),
                    ),
                  );
                  setState(() {
                    isVisible = value;
                  });
                  var params = {
                    'isVisible': isVisible.toString(),
                    'userId': userId ?? "",
                  };
                  print(params);
                  prefs.setBool('isVisible', isVisible);

                  await http.post('$baseUrl/user/visible', body: params);
                  _analytics.logVisibility(isVisible: value);
                },
                activeColor: Color(0xFF29CAE4),
                activeToggleColor: primaryBlue,
                inactiveToggleColor: primaryBlue,
                height: 25,
                width: 40,
                toggleSize: 15,
              ),
            ),
          ),
        ],
        bottom: PreferredSize(
          preferredSize: Size.fromHeight(70),
          child: Theme(
            data: ThemeData(
              splashColor: Colors.transparent,
              highlightColor: Colors.transparent,
            ),
            child: Padding(
              padding: const EdgeInsets.only(left: 20),
              child: Align(
                alignment: Alignment.centerLeft,
                child: TabBar(
                  controller: _tabController,
                  isScrollable: true,
                  labelPadding: EdgeInsets.all(0),
                  indicator: BoxDecoration(
                    borderRadius: BorderRadius.circular(20),
                    color: Color(0xFF29CAE4),
                  ),
                  indicatorSize: TabBarIndicatorSize.label,
                  unselectedLabelColor:
                      Theme.of(context).textTheme.bodyText1.color,
                  tabs: [
                    Container(
                      padding:
                          EdgeInsets.symmetric(horizontal: 20, vertical: 10),
                      child: Text('Search Around'),
                    ),
                    Container(
                      padding:
                          EdgeInsets.symmetric(horizontal: 20, vertical: 10),
                      child: Text('My Profile'),
                    ),
                  ],
                ),
              ),
            ),
          ),
        ),
      ),
      backgroundColor: primaryBlue,
      body: Container(
        margin: EdgeInsets.only(top: 30),
        // padding: EdgeInsets.symmetric(vertical: 35, horizontal: 25),
        decoration: BoxDecoration(
          color: Colors.white,
          borderRadius: BorderRadius.only(
            topLeft: Radius.circular(53),
            topRight: Radius.circular(53),
          ),
        ),
        child: ClipRRect(
          borderRadius: BorderRadius.only(
            topLeft: Radius.circular(53),
            topRight: Radius.circular(53),
          ),
          child: TabBarView(
            controller: _tabController,
            physics: NeverScrollableScrollPhysics(),
            children: [
              isVisible
                  ? SearchPage()
                  : Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        Text(
                          'go online to search',
                          style: TextStyle(
                              color: Theme.of(context)
                                  .textTheme
                                  .bodyText1
                                  .color
                                  .withOpacity(0.2)),
                        ),
                        SizedBox(height: 10),
                        ClipRRect(
                          borderRadius: BorderRadius.circular(20),
                          child: Image(
                            image: AssetImage('images/toggle.gif'),
                            width: 100,
                          ),
                        )
                      ],
                    ),
              MyProfilePage(
                onGetName: (name) {
                  setState(() {
                    this.name = name;
                    greet = "Hello, $name";
                  });
                },
              ),
            ],
          ),
        ),
      ),
    );
  }

  void _onStart() async {
    if (await _checkLocationPermission()) {
      _startLocator();
      final _isRunning = await BackgroundLocator.isServiceRunning();

      setState(() {
        isRunning = _isRunning;
        print("isRunning: $isRunning");
      });
    } else {
      // show error
    }
  }

  Future<bool> _checkLocationPermission() async {
    final access = await LocationPermissions().checkPermissionStatus();
    switch (access) {
      case PermissionStatus.unknown:
      case PermissionStatus.denied:
      case PermissionStatus.restricted:
        await showDialog(
          context: context,
          builder: (context) => AlertDialog(
            title: Text("Allow Location Permission"),
            content: Text(
                "To achieve best experience using FerFax, please allow the app to access location all the time through the next prompt after you click ok"),
            actions: [
              TextButton(
                  onPressed: () async {
                    Navigator.pop(context);
                  },
                  child: Text("OK")),
            ],
          ),
        );

        final permission = await LocationPermissions().requestPermissions(
          permissionLevel: LocationPermissionLevel.locationAlways,
        );
        if (permission == PermissionStatus.granted) {
          return true;
        } else {
          return false;
        }
        break;
      case PermissionStatus.granted:
        return true;
        break;
      default:
        return false;
        break;
    }
  }

  void _startLocator() async {
    BackgroundLocator.registerLocationUpdate(
      LocationCallbackHandler.callback,
      initCallback: LocationCallbackHandler.initCallback,
      disposeCallback: LocationCallbackHandler.disposeCallback,
      autoStop: false,
      androidSettings: AndroidSettings(
        accuracy: LocationAccuracy.NAVIGATION,
        interval: 5,
        wakeLockTime: 120, //minutes
        distanceFilter: 0,
        client: LocationClient.google,
        androidNotificationSettings: AndroidNotificationSettings(
          notificationMsg: "",
          notificationTitle: "",
          notificationBigMsg: "",
        ),
      ),
    );
  }
}

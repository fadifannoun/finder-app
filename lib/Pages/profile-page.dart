import 'dart:convert';

import 'package:clipboard/clipboard.dart';
import 'package:findix_app/Components/social-card.dart';
import 'package:findix_app/utils/constants.dart';
import 'package:findix_app/Components/custom-box-shadow.dart';
import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:url_launcher/url_launcher.dart';
import '../utils/api.dart';
import '../utils/storage.dart' as storage;
import 'package:http/http.dart' as http;
import 'package:findix_app/Components/pop-notification.dart';
import 'package:findix_app/utils/keys.dart' as storageKeys;

final api = API();

class ProfilePage extends StatefulWidget {
  final String id;
  final String firstName;
  final String lastName;
  final String coverImage;
  final String facebookId;
  final String twitterId;
  final String instagramId;
  final String snapchatId;
  final String tiktokId;
  final String youtubeId;
  final String githubId;
  final String linkedinId;
  final String behanceId;
  final String whatsAppId;
  final String pinterestId;
  final String twitchId;
  final String tinderId;
  final String weChatId;
  final String soundCloudId;
  final String spotifyId;
  final String telegramId;
  final String email;
  final String website;
  final String address;
  final String mobileNumber;
  final int distance;
  final String occupation;
  int followers;
  final bool isFavorite;
  final void Function(bool fav) onPop;

  ProfilePage({
    this.id,
    this.firstName,
    this.lastName,
    this.coverImage,
    this.facebookId,
    this.twitterId,
    this.instagramId,
    this.snapchatId,
    this.tiktokId,
    this.youtubeId,
    this.githubId,
    this.linkedinId,
    this.behanceId,
    this.whatsAppId,
    this.pinterestId,
    this.twitchId,
    this.tinderId,
    this.weChatId,
    this.soundCloudId,
    this.spotifyId,
    this.telegramId,
    this.email,
    this.website,
    this.address,
    this.mobileNumber,
    this.isFavorite,
    this.distance,
    this.onPop,
    this.followers,
    this.occupation,
  });

  @override
  _ProfilePageState createState() => _ProfilePageState();
}

const double bodyHeight = 140;

class _ProfilePageState extends State<ProfilePage> {
  bool _isFavorite = false;

  @override
  void initState() {
    print("profile init");
    super.initState();
    setState(() {
      _isFavorite = widget.isFavorite;
    });
  }

  @override
  void dispose() async {
    super.dispose();

    print('disposed');
  }

  @override
  Widget build(BuildContext context) {
    List<SocialCard> socialAccounts = [
      widget.facebookId != null
          ? SocialCard(
              title: 'Facebook',
              icon: FontAwesomeIcons.facebookF,
              type: 'social',
              callback: () async {
                api.openFacebookProfile(widget.facebookId);
              })
          : null,
      widget.instagramId != null
          ? SocialCard(
              title: 'Instagram',
              icon: FontAwesomeIcons.instagram,
              type: 'social',
              callback: () async {
                await api.openInstagramProfile(widget.instagramId);
              },
            )
          : null,
      widget.youtubeId != null
          ? SocialCard(
              title: 'YouTube',
              icon: FontAwesomeIcons.youtube,
              type: 'social',
              callback: () async {
                await api.openYoutubeProfile(widget.youtubeId);
              },
            )
          : null,
      widget.tiktokId != null
          ? SocialCard(
              title: 'TikTok',
              icon: FontAwesomeIcons.tiktok,
              type: 'social',
              callback: () async {
                await api.openTiktokProfile(widget.tiktokId);
              },
            )
          : null,
      widget.linkedinId != null
          ? SocialCard(
              title: 'LinkedIn',
              icon: FontAwesomeIcons.linkedinIn,
              type: 'social',
              callback: () async {
                await api.openLinkedinProfile(widget.linkedinId);
              },
            )
          : null,
      widget.twitterId != null
          ? SocialCard(
              title: 'Twitter',
              icon: FontAwesomeIcons.twitter,
              type: 'social',
              callback: () async {
                await api.openTwitterProfile(widget.twitterId);
              },
            )
          : null,
      widget.snapchatId != null
          ? SocialCard(
              title: 'Snapchat',
              icon: FontAwesomeIcons.snapchatGhost,
              type: 'social',
              callback: () async {
                await api.openSnapchatProfile(widget.snapchatId);
              })
          : null,
      widget.behanceId != null
          ? SocialCard(
              title: 'Behance',
              icon: FontAwesomeIcons.behance,
              type: 'social',
              callback: () async {
                await api.openBehanceProfile(widget.behanceId);
              },
            )
          : null,
      widget.githubId != null
          ? SocialCard(
              title: 'Github',
              icon: FontAwesomeIcons.github,
              type: 'social',
              callback: () async {
                await api.openGithubProfile(widget.githubId);
              },
            )
          : null,
      widget.whatsAppId != null
          ? SocialCard(
              title: 'WhatsApp',
              icon: FontAwesomeIcons.whatsapp,
              type: 'social',
              callback: () async {
                await api.openWhatsAppProfile(widget.whatsAppId);
              },
            )
          : null,
      widget.pinterestId != null
          ? SocialCard(
              title: 'Pinterest',
              icon: FontAwesomeIcons.pinterestP,
              type: 'social',
              callback: () async {
                await api.openPinterestProfile(widget.pinterestId);
              },
            )
          : null,
      widget.twitchId != null
          ? SocialCard(
              title: 'Twitch',
              icon: FontAwesomeIcons.twitch,
              type: 'social',
              callback: () async {
                await api.openTwitchProfile(widget.twitchId);
              },
            )
          : null,
      widget.tinderId != null
          ? SocialCard(
              title: 'Tinder',
              icon: FontAwesomeIcons.fireAlt,
              type: 'social',
              callback: () async {
                await api.openTinderProfile(widget.tinderId);
              },
            )
          : null,
      widget.weChatId != null
          ? SocialCard(
              title: 'WeChat',
              icon: FontAwesomeIcons.weixin,
              type: 'social',
              callback: () async {
                await api.openWeChatProfile(widget.weChatId);
              },
            )
          : null,
      widget.soundCloudId != null
          ? SocialCard(
              title: 'SoundCloud',
              icon: FontAwesomeIcons.soundcloud,
              type: 'social',
              callback: () async {
                await api.openSoundCloudProfile(widget.soundCloudId);
              },
            )
          : null,
      widget.spotifyId != null
          ? SocialCard(
              title: 'Spotify',
              icon: FontAwesomeIcons.spotify,
              type: 'social',
              callback: () async {
                await api.openSpotifyProfile(widget.spotifyId);
              },
            )
          : null,
      widget.telegramId != null
          ? SocialCard(
              title: 'Telegram',
              icon: FontAwesomeIcons.telegramPlane,
              type: 'social',
              callback: () async {
                await api.openTelegramProfile(widget.telegramId);
              },
            )
          : null,
    ];
    List<SocialCard> contactInfo = [
      widget.email != null
          ? SocialCard(
              title: 'Email',
              icon: FontAwesomeIcons.envelope,
              callback: () async {
                showModal(widget.email, 'Email', FontAwesomeIcons.envelope);
              },
            )
          : null,
      widget.website != null
          ? SocialCard(
              title: 'Website',
              icon: FontAwesomeIcons.globeAfrica,
              callback: () async {
                showModal(
                    widget.website, 'Website', FontAwesomeIcons.globeAfrica);
              },
            )
          : null,
      widget.address != null
          ? SocialCard(
              title: 'Address',
              icon: FontAwesomeIcons.mapMarkerAlt,
              callback: () async {
                showModal(
                    widget.address, 'Address', FontAwesomeIcons.mapMarkerAlt);
              },
            )
          : null,
      widget.mobileNumber != null
          ? SocialCard(
              title: 'Mobile',
              icon: FontAwesomeIcons.mobileAlt,
              callback: () async {
                showModal(
                    widget.mobileNumber, 'Mobile', FontAwesomeIcons.mobileAlt);
              },
            )
          : null,
    ];
    int socialLength =
        socialAccounts.where((element) => element != null).toList().length;
    int infoLength =
        contactInfo.where((element) => element != null).toList().length;

    return WillPopScope(
      onWillPop: () async {
        widget.onPop(_isFavorite);
        Navigator.pop(context, _isFavorite);
        return true;
      },
      child: Scaffold(
        body: Stack(
          children: [
            Container(
              width: double.infinity,
              child: Image(
                image: (widget.coverImage == "" || widget.coverImage == null)
                    ? AssetImage('images/cover.png')
                    : NetworkImage("${widget.coverImage}"),
                fit: BoxFit.fitWidth,
                errorBuilder: (BuildContext context, Object exception,
                    StackTrace stackTrace) {
                  return Image.asset(
                    'images/cover.png',
                    fit: BoxFit.fill,
                  );
                },
                // height: 40,
              ),
            ),
            Container(
              height: bodyHeight + 65,
              decoration: BoxDecoration(
                boxShadow: [
                  CustomBoxShadow(
                    offset: Offset(0, -6),
                    blurRadius: 30,
                    color: Colors.white.withOpacity(0.16),
                  ),
                ],
                gradient: LinearGradient(
                  begin: Alignment.topCenter,
                  end: Alignment.bottomCenter,
                  colors: [Color(0xFFD8D8D8).withOpacity(0.4), primaryBlue],
                ),
              ),
            ),
            Container(
              padding: EdgeInsets.symmetric(vertical: 35),
              child: Row(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Container(
                    child: BackButton(color: Colors.white),
                  ),
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text(
                        '${widget.firstName} ${widget.lastName ?? ""}',
                        style: TextStyle(
                            color: Colors.white,
                            fontSize: 28,
                            fontWeight: FontWeight.w600),
                      ),
                      Text(
                        widget.occupation ?? "",
                        style: TextStyle(
                            color: Colors.white,
                            fontSize: 12,
                            fontWeight: FontWeight.w400),
                      ),
                      Text(
                        '${widget.followers} people like ${widget.firstName}',
                        style: TextStyle(
                          color: Colors.white,
                          fontSize: 12,
                          fontWeight: FontWeight.w400,
                        ),
                      ),
                    ],
                  ),
                ],
              ),
            ),
            Container(
              height: double.infinity,
              margin: EdgeInsets.only(top: bodyHeight),
              padding: EdgeInsets.symmetric(horizontal: 0),
              decoration: BoxDecoration(
                borderRadius: BorderRadius.only(topRight: Radius.circular(53)),
                color: Colors.white,
              ),
              child: ClipRRect(
                borderRadius: BorderRadius.only(topRight: Radius.circular(53)),
                child: SingleChildScrollView(
                  child: Container(
                    padding: EdgeInsets.symmetric(horizontal: 20),
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        SizedBox(height: 35),
                        socialLength > 0
                            ? Container(
                                child: Column(
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  children: [
                                    Text(
                                      "Available Social Accounts",
                                      style: TextStyle(
                                        fontSize: 16,
                                        fontWeight: FontWeight.w600,
                                        color: Color(0xFF362D70),
                                      ),
                                    ),
                                    SizedBox(height: 5),
                                    Text(
                                      "Select one to show on native app or via browser",
                                      style: TextStyle(
                                        fontSize: 13,
                                        fontWeight: FontWeight.w400,
                                        color: secondaryBlue.withOpacity(0.5),
                                      ),
                                    ),
                                    // SizedBox(height: 20),
                                    GridView.count(
                                      clipBehavior: Clip.none,
                                      crossAxisCount: 3,
                                      padding: EdgeInsets.only(top: 30),
                                      shrinkWrap: true,
                                      physics: NeverScrollableScrollPhysics(),
                                      children: socialAccounts
                                          .where((element) => element != null)
                                          .toList(),
                                    ),
                                  ],
                                ),
                              )
                            : Container(),
                        infoLength > 0
                            ? Container(
                                child: Column(
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  children: [
                                    SizedBox(height: 40),
                                    Text(
                                      'Contact Info',
                                      style: TextStyle(
                                        fontSize: 16,
                                        fontWeight: FontWeight.w600,
                                        color: Color(0xFF362D70),
                                      ),
                                    ),
                                    // SizedBox(height: 20),
                                    GridView.count(
                                      shrinkWrap: true,
                                      clipBehavior: Clip.none,
                                      padding: EdgeInsets.only(top: 30),
                                      physics: NeverScrollableScrollPhysics(),
                                      crossAxisCount: 3,
                                      children: contactInfo
                                          .where((element) => element != null)
                                          .toList(),
                                    )
                                  ],
                                ),
                              )
                            : Container(),
                      ],
                    ),
                  ),
                ),
              ),
            ),
            Positioned(
              child: FloatingActionButton(
                onPressed: () async {
                  setState(() {
                    _isFavorite = !_isFavorite;
                    print('$_isFavorite');
                  });
                  var currentUser = await storage.read(storageKeys.userId);
                  var params = {
                    'userId': currentUser,
                  };
                  if (_isFavorite == true) {
                    var likedUser = widget.id;
                    params['likedUser'] = likedUser;
                  } else {
                    var unlikedUser = widget.id;
                    params['unlikedUser'] = unlikedUser;
                  }
                  var response =
                      await http.post('$baseUrl/user/favorite', body: params);
                  var data = jsonDecode(response.body)['data'];
                  int followers = data['followers'];
                  setState(() {
                    widget.followers = followers;
                  });
                },
                child: FaIcon(FontAwesomeIcons.heart,
                    color: _isFavorite ? Colors.white : Color(0xFF32286D)),
                backgroundColor: _isFavorite ? primaryBlue : Colors.white,
              ),
              right: 35,
              top: bodyHeight - 30,
            ),
          ],
        ),
      ),
    );
  }

  void showModal(value, title, icon) {
    TextEditingController _controller = TextEditingController()..text = value;
    showDialog<void>(
      barrierDismissible: true,
      context: context,
      barrierColor: primaryBlue.withOpacity(0.75),
      builder: (BuildContext context) {
        return Scaffold(
          backgroundColor: Colors.transparent,
          body: Builder(
            builder: (context) => Theme(
              data: Theme.of(context)
                  .copyWith(iconTheme: IconThemeData(color: Color(0xFF32286D))),
              child: Stack(
                children: [
                  GestureDetector(
                    onTap: () {
                      Navigator.pop(context);
                    },
                  ),
                  Container(
                    decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(20),
                      color: Colors.white,
                    ),
                    height: 160,
                    alignment: Alignment.topCenter,
                    margin: EdgeInsets.symmetric(horizontal: 20, vertical: 105),
                    padding: EdgeInsets.symmetric(horizontal: 20, vertical: 15),
                    child: Column(
                      mainAxisSize: MainAxisSize.min,
                      children: [
                        Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: [
                            Container(
                              child: Row(
                                children: [
                                  FaIcon(icon),
                                  SizedBox(width: 8),
                                  Text(title),
                                ],
                              ),
                            ),
                            IconButton(
                              icon: FaIcon(
                                FontAwesomeIcons.timesCircle,
                                color: Color(0xFFAAAAAA),
                              ),
                              onPressed: () {
                                Navigator.pop(context);
                              },
                            ),
                          ],
                        ),
                        Row(
                          children: [
                            Expanded(
                              child: GestureDetector(
                                onTap: () async {
                                  switch (title) {
                                    case "Website":
                                      _openUrl("http://${widget.website}");
                                      break;
                                    case "Email":
                                      _openUrl("mailto:${widget.email}");
                                      break;
                                    case "Mobile":
                                      _openUrl("tel:${widget.mobileNumber}");
                                      break;
                                    default:
                                      print("invalid operation");
                                  }
                                },
                                child: TextFormField(
                                  controller: _controller,
                                  enabled: false,
                                  decoration: InputDecoration(
                                    border: OutlineInputBorder(),
                                  ),
                                ),
                              ),
                            ),
                            IconButton(
                              icon: FaIcon(FontAwesomeIcons.copy),
                              onPressed: () {
                                FlutterClipboard.copy(_controller.text)
                                    .then((value) => print('copied'));
                                final notification = PopNotification(
                                    "Text copied to clipboard.");

                                // Find the Scaffold in the widget tree and use
                                // it to show a SnackBar.
                                Scaffold.of(context).showSnackBar(notification);
                              },
                            ),
                          ],
                        )
                      ],
                    ),
                  ),
                ],
              ),
            ),
          ),
        );
      },
    );
  }
}

Future<void> _openUrl(String url) async {
  if (await canLaunch(url)) {
    await launch(url);
  } else {
    throw 'Could not launch $url';
  }
}

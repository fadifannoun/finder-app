import 'package:background_locator/location_dto.dart';
import 'package:findix_app/Pages/auth/register-page.dart';
import 'package:findix_app/utils/constants.dart';
import 'package:findix_app/Components/custom-box-shadow.dart';
import 'package:findix_app/utils/storage.dart' as storage;
import 'package:flutter/material.dart';
import 'package:findix_app/Components/nearby-user.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'profile-page.dart';
import 'package:http/http.dart' as http;
import 'dart:convert';
import 'package:findix_app/utils/keys.dart' as storageKeys;

class SearchPage extends StatefulWidget {
  @override
  _SearchPageState createState() => _SearchPageState();
}

class _SearchPageState extends State<SearchPage>
    with AutomaticKeepAliveClientMixin {
  int _finalLocation = 0;
  int _locationRange = 0;
  List<NearbyUser> nearbyUsers = [];
  String resultMessage = "";
  bool isLoading = false;
  String userId;
  SharedPreferences prefs;

  //it is wrong to keep it alive always, instead, use updateKeepAlive()
  @override
  bool get wantKeepAlive => true;

  Future<void> init() async {
    var id = await storage.read(storageKeys.userId);
    prefs = await SharedPreferences.getInstance();

    setState(() {
      userId = id;
    });
  }

  loadResults() async {
    setState(() {
      print('loading true');
      isLoading = true;
    });
    await prefs.reload();
    var locationString = prefs.getString("location");
    if (locationString != null) {
      var response;
      try {
        LocationDto location = LocationDto.fromJson(jsonDecode(locationString));

        response = await http.post('$baseUrl/user/nearby-users', body: {
          'distance': "$_finalLocation",
          "location": jsonEncode({
            'longitude': location.longitude,
            'latitude': location.latitude,
          }),
          "userId": userId ?? "",
        });
        print("response received");
      } catch (e) {
        print(e);
      } finally {
        setState(() {
          print('loading false');
          isLoading = false;
        });
      }

      // print(jsonDecode(response.body)['data']['users']);
      List users = jsonDecode(response.body)['data']['users'];
      // print(users);
      List<NearbyUser> usersWidgetList = [];
      users.asMap().forEach((i, user) {
        // print(user['isFavorite'] ?? 'not null');

        usersWidgetList.add(
          NearbyUser(
            firstName: user['firstName'],
            lastName: user['lastName'],
            distance: user['distance'].round(),
            occupation: user['occupation'],
            // urlImage: element['user']['imageUrl'],
            image: user['profileImage'],
            onClick: () async {
              if (userId != null) {
                await Navigator.push(
                  context,
                  MaterialPageRoute(
                    settings: RouteSettings(
                      name: "NearbyProfilePage",
                    ),
                    builder: (context) => ProfilePage(
                      id: user['_id'],
                      firstName: user['firstName'],
                      lastName: user['lastName'],
                      coverImage: user['coverImage'],
                      facebookId: user['facebookId'],
                      twitterId: user['twitterId'],
                      youtubeId: user['youtubeId'],
                      githubId: user['githubId'],
                      instagramId: user['instagramId'],
                      tiktokId: user['tiktokId'],
                      snapchatId: user['snapchatId'],
                      linkedinId: user['linkedinId'],
                      behanceId: user['behanceId'],
                      whatsAppId: user['whatsAppId'],
                      pinterestId: user['pinterestId'],
                      twitchId: user['twitchId'],
                      tinderId: user['tinderId'],
                      weChatId: user['weChatId'],
                      soundCloudId: user['soundCloudId'],
                      spotifyId: user['spotifyId'],
                      telegramId: user['telegramId'],
                      email: user['email'],
                      website: user['website'],
                      mobileNumber: user['mobileNumber'],
                      isFavorite: user['isFavorite'] ?? false,
                      distance: user['distance'].round(),
                      followers: user['followers'],
                      occupation: user['occupation'],
                      onPop: (fav) {
                        print("triggered");
                        users[i]['isFavorite'] = fav;
                      },
                      //todo: fill all fields
                    ),
                  ),
                );

                // if (result == false) {
                //   users[i]['isFavorite'] = false;
                // } else {
                //   users[i]['isFavorite'] = true;
                // }
              } else {
                showDialog(
                  context: context,
                  builder: (BuildContext context) {
                    return AlertDialog(
                      title: Text('You must register to view profiles'),
                      actions: [
                        TextButton(
                          onPressed: () async {
                            Navigator.pushReplacement(
                                context,
                                MaterialPageRoute(
                                  builder: (context) => RegisterPage(),
                                  settings: RouteSettings(
                                    name: "RegisterPage",
                                  ),
                                ));
                          },
                          child: Text('Go to register page'),
                        ),
                        TextButton(
                          onPressed: () {
                            Navigator.pop(context);
                          },
                          child: Text('cancel'),
                        ),
                      ],
                    );
                  },
                );
              }
            },
          ),
        );
      });

      setState(() {
        nearbyUsers = usersWidgetList;
        if (nearbyUsers.length == 0) {
          resultMessage = "No devices detected in this region";
        } else if (nearbyUsers.length == 1) {
          resultMessage =
              'Awesome! Found ${nearbyUsers.length} person around you!';
        } else {
          resultMessage =
              'Awesome! Found ${nearbyUsers.length} persons around you!';
        }
      });
    } else {
      print("failed to get location");
    }
  }

  @override
  void initState() {
    init();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.only(top: 35),
      // padding: EdgeInsets.symmetric(vertical: 35, horizontal: 25),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Container(
            padding: EdgeInsets.symmetric(horizontal: 25),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Text('Where are you looking?'),
                    _finalLocation > 0
                        ? Text(
                            '${_finalLocation}m',
                            style: TextStyle(
                              color: Color(0xFF29CAE4),
                            ),
                          )
                        : Text(""),
                  ],
                ),
                SizedBox(height: 12),
                Container(
                  padding: EdgeInsets.symmetric(horizontal: 20, vertical: 10),
                  decoration: BoxDecoration(
                    color: Colors.white,
                    borderRadius: BorderRadius.all(Radius.circular(12)),
                    boxShadow: [
                      CustomBoxShadow(
                        color: Colors.black.withOpacity(0.14),
                        blurRadius: 15,
                      )
                    ],
                  ),
                  child: Column(
                    children: [
                      Padding(
                        padding: const EdgeInsets.symmetric(horizontal: 8),
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: [Text('0 m'), Text('1000 m')],
                        ),
                      ),
                      SliderTheme(
                        data: SliderThemeData(
                          activeTrackColor: primaryBlue,
                          inactiveTrackColor: Color(0xFFE5E5E5),
                          thumbColor: primaryBlue,
                          overlayColor: primaryBlue.withOpacity(0.5),
                          overlayShape: RoundSliderOverlayShape(
                            overlayRadius: 16.0,
                          ),
                          thumbShape: RoundSliderThumbShape(
                            enabledThumbRadius: 6.0,
                          ),
                        ),
                        child: Slider(
                          value: _locationRange.toDouble(),
                          min: 0,
                          max: 1000,
                          divisions: 1000,
                          label: _locationRange.round().toString(),
                          onChanged: (newValue) {
                            setState(() {
                              _locationRange = newValue.round();
                            });
                          },
                          onChangeEnd: (value) async {
                            setState(() {
                              _finalLocation = value.round();
                            });
                            loadResults();
                          },
                        ),
                      ),
                    ],
                  ),
                ),
              ],
            ),
          ),
          Expanded(
            child: _finalLocation == 0
                ? Center(
                    child: Text(
                      'Select Range\nTo See People Around',
                      textAlign: TextAlign.center,
                      style: TextStyle(
                          color: Theme.of(context)
                              .textTheme
                              .bodyText1
                              .color
                              .withOpacity(0.2)),
                    ),
                  )
                : isLoading
                    ? Center(
                        child: Column(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: [
                            CircularProgressIndicator(
                              valueColor:
                                  AlwaysStoppedAnimation<Color>(primaryBlue),
                            ),
                            SizedBox(height: 10),
                            Text(
                              "Searching Around",
                              style: TextStyle(
                                fontSize: 16,
                                fontWeight: FontWeight.w400,
                                color: secondaryBlue.withOpacity(0.2),
                              ),
                            ),
                          ],
                        ),
                      )
                    : nearbyUsers.length > 0
                        ? SingleChildScrollView(
                            physics: AlwaysScrollableScrollPhysics(),
                            child: Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                SizedBox(
                                  height: 18,
                                ),
                                Container(
                                  padding: EdgeInsets.symmetric(horizontal: 25),
                                  child: Text(
                                    resultMessage,
                                    style: TextStyle(
                                        fontSize: 13,
                                        fontWeight: FontWeight.w400,
                                        color: Theme.of(context)
                                            .textTheme
                                            .bodyText1
                                            .color
                                            .withOpacity(.5)),
                                  ),
                                ),
                                SizedBox(height: 10),
                                ...nearbyUsers,
                              ],
                            ),
                          )
                        : Center(
                            child: Text(
                              'No one online yet!\nTry to move to another place :)',
                              textAlign: TextAlign.center,
                              style: TextStyle(
                                  color: Theme.of(context)
                                      .textTheme
                                      .bodyText1
                                      .color
                                      .withOpacity(0.2)),
                            ),
                          ),
          ),
        ],
      ),
    );
  }
}

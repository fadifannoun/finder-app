import 'dart:async';
import 'dart:convert';

import 'package:findix_app/utils/constants.dart';
import 'package:http/http.dart' as http;
import 'package:background_locator/location_dto.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'storage.dart' as storage;

class LocationServiceRepository {
  static LocationServiceRepository _instance = LocationServiceRepository._();
  SharedPreferences prefs;
  LocationServiceRepository._();

  factory LocationServiceRepository() {
    return _instance;
  }

  static const String isolateName = 'LocatorIsolate';

  int _count = -1;
  String userId;

  Future<void> init(Map<dynamic, dynamic> params) async {
    print("***********Init callback handler");
    // userId = await read('userId');

    _count = 0;
    print("$_count");
  }

  Future<void> dispose() async {
    print("***********Dispose callback handler");
    print("$_count");
  }

  Future<void> callback(LocationDto locationDto) async {
    // final SendPort send = IsolateNameServer.lookupPortByName(isolateName);
    // send?.send(locationDto);
    String locJson = jsonEncode(locationDto);
    print(locJson);
    prefs = await SharedPreferences.getInstance();

    await prefs.setString("location", locJson);

    userId = await storage.read('userId');
    var params = {
      "location": locJson,
      "userId": userId ?? "",
    };

    // print('params:$params');
    await http.post("$baseUrl/user/location", body: params);

    print('$_count');
    _count++;
  }
}

import 'package:flutter_secure_storage/flutter_secure_storage.dart';

final _storage = new FlutterSecureStorage();

Future<void> store(key, value) async {
  await _storage.write(key: key, value: value);
  print('stored');
}

Future<String> read(key) async {
  return await _storage.read(key: key);
}

Future<void> clear() async {
  await _storage.deleteAll();
}

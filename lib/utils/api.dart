import 'dart:async';
import 'dart:convert';
import 'package:findix_app/utils/constants.dart';
import 'package:http/http.dart' as http;
import 'package:flutter_appauth/flutter_appauth.dart';
import 'package:twitter_login/twitter_login.dart';
import 'package:snapchat_flutter_plugin/snapchat_flutter_plugin.dart';
import 'package:url_launcher/url_launcher.dart';
import 'storage.dart' as storage;

FlutterAppAuth appAuth = FlutterAppAuth();

//handle on remove/reinstall, with connected apps
class API {
  //can be split into login class and fetch data class
  String _mobileNumber;

  getMobileNumber() async {
    _mobileNumber = await storage.read("mobileNumber");
  }

  API() {
    print("api constructor");
    getMobileNumber();
  }

  Future<Map<String, dynamic>> loginWithFacebook() async {
    final AuthorizationTokenResponse authResponse =
        await appAuth.authorizeAndExchangeCode(
      AuthorizationTokenRequest(
        '299488231502597',
        'https://findix.herokuapp.com/auth',
        serviceConfiguration: AuthorizationServiceConfiguration(
          'https://www.facebook.com/v9.0/dialog/oauth',
          'https://graph.facebook.com/v9.0/oauth/access_token',
        ),
        clientSecret: "0dc62249a4437439045c95c6fa23cf61",
      ),
    );

    var token = authResponse.accessToken;
    final graphResponse = await http.get(
        'https://graph.facebook.com/v9.0/me?fields=name,first_name,last_name,email,picture&access_token=$token');

    //get hi-res picture
    var picResponse = await http.get(
        'https://graph.facebook.com/v9.0/me/picture?redirect=0&type=large&access_token=$token');
    var profilePic = jsonDecode(picResponse.body)["data"]['url'];

    final user = jsonDecode(graphResponse.body);

    //mobileNumber can be replaced with user id
    var params = {
      'social': 'facebook',
      'facebookId': user['id'],
      'firstName': user['first_name'],
      'lastName': user['last_name'],
      'image': profilePic,
      'mobileNumber': _mobileNumber,
    };

    return params;
  }

  Future<Map<String, dynamic>> loginWithInstagram() async {
    print('insta login');

    final AuthorizationResponse codeResponse = await appAuth.authorize(
      AuthorizationRequest(
        "308594980450310",
        'https://findix.herokuapp.com/auth',
        serviceConfiguration: AuthorizationServiceConfiguration(
          'https://api.instagram.com/oauth/authorize',
          'https://api.instagram.com/oauth/access_token',
        ),
        scopes: ["user_profile"],
      ),
    );
    var code = codeResponse.authorizationCode;

    var tokenResponse = await http.post(
      "https://api.instagram.com/oauth/access_token",
      body: {
        "client_id": "308594980450310",
        "client_secret": "b2bbcc44723561b139582fe38880627c",
        "code": "$code",
        "grant_type": "authorization_code",
        "redirect_uri": "https://findix.herokuapp.com/auth",
      },
    );
    var token = jsonDecode(tokenResponse.body)['access_token'];

    var graphResponse = await http.get(
      "https://graph.instagram.com/me" +
          "?fields=username" +
          "&access_token=$token",
    );
    var user = jsonDecode(graphResponse.body);
    print(user);

    var params = {
      'social': 'instagram',
      'mobileNumber': _mobileNumber,
      'instagramId': user["username"],
      'username': user["username"],
    };
    return params;
  }

  Future<Map<String, dynamic>> loginWithYoutube() async {
    print('yt login');

    final AuthorizationTokenResponse authResponse =
        await appAuth.authorizeAndExchangeCode(
      AuthorizationTokenRequest(
        '1078408644713-j6s5qd3gh8n9ecbbph4e7vbj3cb0940n.apps.googleusercontent.com',
        'https://findix.herokuapp.com/auth',
        serviceConfiguration: AuthorizationServiceConfiguration(
          'https://accounts.google.com/o/oauth2/v2/auth',
          'https://oauth2.googleapis.com/token',
        ),
        scopes: [
          "https://www.googleapis.com/auth/userinfo.profile",
          'https://www.googleapis.com/auth/youtube'
        ],
        clientSecret: "1h-vV-0uGj11WSihXn4H-r4p",
      ),
    );

    var token = authResponse.accessToken;

    var googleResponse =
        await http.get("https://www.googleapis.com/userinfo/v2/me", headers: {
      "Connection": "Keep-Alive",
      "Authorization": "Bearer $token",
    });
    var user = jsonDecode(googleResponse.body);
    print(user);

    var youtubeResponse = await http.get(
        'https://youtube.googleapis.com/youtube/v3/channels?mine=true',
        headers: {
          "Connection": "Keep-Alive",
          "Authorization": "Bearer $token",
        });
    var channel = jsonDecode(youtubeResponse.body);

    var params = {
      'social': "youtube",
      'youtubeId': channel['items'][0]['id'],
      'username': channel['items'][0]['id'],
      'mobileNumber': _mobileNumber,
      'firstName': user['given_name'],
      'lastName': user['family_name'],
      'image': user['picture'],
    };
    return params;
  }

  Future<Map<String, dynamic>> loginWithTiktok() async {
    print('tt login');
    var params = {
      'social': 'tiktok',
      'mobileNumber': _mobileNumber,
    };
    return params;
  }

  Future<Map<String, dynamic>> loginWithLinkedin() async {
    print('in login');
    //needs vanity name

    final AuthorizationResponse codeResponse = await appAuth.authorize(
      AuthorizationRequest(
        "78lucc5skoxh9m",
        "https://findix.herokuapp.com/auth",
        serviceConfiguration: AuthorizationServiceConfiguration(
          'https://www.linkedin.com/oauth/v2/authorization',
          "https://www.linkedin.com/oauth/v2/accessToken",
        ),
        scopes: ["r_liteprofile"],
      ),
    );

    var tokenResponse = await http.post(
      "https://www.linkedin.com/oauth/v2/accessToken",
      headers: {
        "Content-Type": "application/x-www-form-urlencoded",
      },
      body: {
        "grant_type": "authorization_code",
        "code": "${codeResponse.authorizationCode}",
        "redirect_uri": "https://findix.herokuapp.com/auth",
        "client_id": "78lucc5skoxh9m",
        "client_secret": "HExgdCfFuGY6rBtr",
      },
    );
    var token = jsonDecode(tokenResponse.body)["access_token"];

    var linkedinResponse =
        await http.get('https://api.linkedin.com/v2/me', headers: {
      "Connection": "Keep-Alive",
      "Authorization": "Bearer $token",
    });
    var user = jsonDecode(linkedinResponse.body);

    var params = {
      'social': "linkedin",
      'linkedinId': user["id"],
      'mobileNumber': _mobileNumber,
      'firstName': user['localizedFirstName'],
      'lastName': user["localizedLastName"],
      // 'image': user["profilePicture"],
    };
    return params;
  }

  Future<Map<String, dynamic>> loginWithTwitter() async {
    //api key: WdAh7zgYPUfuDZD3X6xZUtRL2
    //api secret key: WRfkCHVNl7wqn6CtS7qytfGz2MjZdzrkW8v6cpqx2UgaD7BR5e
    // bearer token: AAAAAAAAAAAAAAAAAAAAACpFLAEAAAAAhjHHIYMPOEpglGxRjZNGIQkIr1E%3Du0hR3NeLXHZJfOUp42S1aNNmg8CQp26vQZuDbLyscMkzpuhoIn
    //app id:19678506
    //access token: 1462119510-A8LES0uqi2dlPP0ljDQXefazIq6fVwZ8pY3zF4O
    //access token secret: yOM5TefRcbkNiF02ffqAbKNHDheC2vJN53t5RfoJPKgxb
    print('tw login');
    final twitterLogin = TwitterLogin(
      apiKey: 'WdAh7zgYPUfuDZD3X6xZUtRL2',
      apiSecretKey: 'WRfkCHVNl7wqn6CtS7qytfGz2MjZdzrkW8v6cpqx2UgaD7BR5e',
      redirectURI: 'findixtw://auth',
    );

    final authResult = await twitterLogin.login();
    switch (authResult.status) {
      case TwitterLoginStatus.loggedIn:
        // success

        var user = authResult.user;
        var fullName = user.name.split(' ');
        var firstName = fullName[0];
        var lastName = "";
        if (fullName.length >= 2) {
          lastName = fullName[fullName.length - 1];
        }

        var params = {
          'social': 'twitter',
          'twitterId': user.screenName,
          'username': user.screenName,
          'mobileNumber': _mobileNumber,
          'firstName': firstName,
          'lastName': lastName,
          'image': user.thumbnailImage,
        };
        return params;

        break;
      case TwitterLoginStatus.cancelledByUser:
        // cancel
        break;
      case TwitterLoginStatus.error:
        // error
        break;
    }
  }

  Future<Map<String, dynamic>> loginWithSnapchat() async {
    print('sc login');

    //client id:1d1af81c-b683-4253-bc38-9879fce73266
    //conf client id:cd92abc2-5f9e-4f27-b5cf-bcdf47b4a2d4
    //secret:V6bGHbgluuuV6evSUI5mMZa9AkX8SPpyVQkVFSrLTww
    //snapkit id:0c7ed784-b130-49b2-800b-5386484d0399

    var data = await SnapchatFlutterPlugin.snapchatLogin;
    print(data);
    var fullName = data["fullName"].split(' ');

    var params = {
      'social': 'snapchat',
      'mobileNumber': _mobileNumber,
      'firstName': fullName[0],
      'lastName': fullName[1],
    };

    return params;

    // var snapchat = SnapchatPlugin();
    // await snapchat.init();

// Make sure that the user has Snapchat installed
//     assert await snapchat.isSnapchatInstalled();

// Use the Login Kit to authenticate the user
//     SnapchatUser user = await snapchat.login();

// Get the cached user
//     user = snapchat.loggedInUser;
// // If there is a cached user, snapchat.login() will automatically return it
//     print('user:${user.displayName}');
  }

  Future<Map<String, dynamic>> loginWithBehance() async {
    print('be login');
    var params = {
      'social': 'behance',
      'mobileNumber': _mobileNumber,
    };
    return params;
  }

  Future<Map<String, dynamic>> loginWithGithub() async {
    print('gh login');
    //client id: 341f437c7b527ec63d1e
    //client secret: d469b5109ffdbaf236e4f8e09543e01cc4bb7a79

    final AuthorizationTokenResponse authResponse =
        await appAuth.authorizeAndExchangeCode(
      AuthorizationTokenRequest(
        '341f437c7b527ec63d1e',
        'findix://auth',
        serviceConfiguration: AuthorizationServiceConfiguration(
            'https://github.com/login/oauth/authorize',
            'https://github.com/login/oauth/access_token'),
        scopes: [
          "user",
          "repo",
          "public_repo",
        ],
        clientSecret: "d469b5109ffdbaf236e4f8e09543e01cc4bb7a79",
      ),
    );
    var token = authResponse.accessToken;

    var githubResponse = await http.get("https://api.github.com/user",
        headers: {"Authorization": "token $token"});

    var user = jsonDecode(githubResponse.body);
    var fullName = user["name"].split(' ');

    var params = {
      'social': "github",
      'githubId': user["login"],
      'username': user["login"],
      'mobileNumber': _mobileNumber,
      'firstName': fullName[0],
      'lastName': fullName[1],
      'image': user["avatar_url"],
    };
    print(params);
    return params;
  }

  Future<Map<String, dynamic>> loginWithWhatsApp() async {
    var params = {
      'social': 'whatsapp',
      'mobileNumber': _mobileNumber,
    };
    return params;
  }

  //todo: add oauth flow
  Future<Map<String, dynamic>> loginWithPinterest() async {
    print('be login');
    var params = {
      'social': 'pinterest',
      'mobileNumber': _mobileNumber,
    };
    return params;
  }

  //todo: add oauth flow
  Future<Map<String, dynamic>> loginWithTwitch() async {
    print('be login');
    var params = {
      'social': 'twitch',
      'mobileNumber': _mobileNumber,
    };
    return params;
  }

  Future<Map<String, dynamic>> loginWithTinder() async {
    print('be login');
    var params = {
      'social': 'tinder',
      'mobileNumber': _mobileNumber,
    };
    return params;
  }

  Future<Map<String, dynamic>> loginWithWeChat() async {
    print('be login');
    var params = {
      'social': 'wechat',
      'mobileNumber': _mobileNumber,
    };
    return params;
  }

  Future<Map<String, dynamic>> loginWithSoundCloud() async {
    print('be login');
    var params = {
      'social': 'soundcloud',
      'mobileNumber': _mobileNumber,
    };
    return params;
  }

  Future<Map<String, dynamic>> loginWithSpotify() async {
    print('be login');
    var params = {
      'social': 'spotify',
      'mobileNumber': _mobileNumber,
    };
    return params;
  }

  Future<Map<String, dynamic>> loginWithTelegram() async {
    print('be login');
    var params = {
      'social': 'telegram',
      'mobileNumber': _mobileNumber,
    };
    return params;
  }

  Future<void> openFacebookProfile(id) async {
    try {
      bool launched =
          await launch(FacebookProtocolURL + id, forceWebView: false);

      if (!launched) {
        await launch(FacebookFallbackURL + id, forceSafariVC: false);
      }
    } catch (e) {
      await launch(FacebookFallbackURL + id, forceSafariVC: false);
    }
  }

  Future<void> openInstagramProfile(id) async {
    try {
      bool launched =
          await launch(InstagramProtocolURL + id, forceWebView: false);

      if (!launched) {
        await launch(InstagramFallbackURL + id, forceSafariVC: false);
      }
    } catch (e) {
      await launch(InstagramFallbackURL + id, forceSafariVC: false);
    }
  }

  Future<void> openYoutubeProfile(id) async {
    if (await canLaunch(YoutubeURL)) {
      await launch(YoutubeURL + id,
          forceWebView: false, enableJavaScript: true);
    } else {
      throw 'Could not launch $YoutubeURL';
    }
  }

  Future<void> openTiktokProfile(id) async {
    if (await canLaunch(TiktokURL)) {
      await launch(TiktokURL + id, forceWebView: false, enableJavaScript: true);
    } else {
      throw 'Could not launch $TiktokURL';
    }
  }

  Future<void> openLinkedinProfile(id) async {
    if (await canLaunch(LinkedinURL)) {
      await launch(LinkedinURL + id,
          forceWebView: false, enableJavaScript: true);
    } else {
      throw 'Could not launch $LinkedinURL';
    }
  }

  Future<void> openTwitterProfile(id) async {
    try {
      bool launched =
          await launch(TwitterProtocolURL + id, forceWebView: false);

      if (!launched) {
        await launch(TwitterFallbackURL + id, forceSafariVC: false);
      }
    } catch (e) {
      print("fallback");
      await launch(TwitterFallbackURL + id, forceSafariVC: false);
    }
  }

  Future<void> openSnapchatProfile(id) async {
    // var url = "snapchat://add/$id";

    if (await canLaunch(SnapchatURL)) {
      await launch(SnapchatURL + id,
          forceWebView: false, enableJavaScript: true);
    } else {
      throw 'Could not launch $SnapchatURL';
    }
  }

  Future<void> openBehanceProfile(id) async {
    if (await canLaunch(BehanceURL)) {
      await launch(BehanceURL + id,
          forceWebView: false, enableJavaScript: true);
    } else {
      throw 'Could not launch $BehanceURL';
    }
  }

  Future<void> openGithubProfile(id) async {
    if (await canLaunch(GithubURL)) {
      await launch(GithubURL + id, forceWebView: false, enableJavaScript: true);
    } else {
      throw 'Could not launch $GithubURL';
    }
    print('get gh data');
  }

  Future<void> openWhatsAppProfile(id) async {
    if (await canLaunch(WhatsappURL)) {
      await launch(WhatsappURL + id,
          forceWebView: false, enableJavaScript: true);
    } else {
      throw 'Could not launch $WhatsappURL';
    }
  }

  Future<void> openPinterestProfile(id) async {
    try {
      bool launched = await launch(PinterestURL + id, forceWebView: false);

      if (!launched) {
        await launch(PinterestFallbackURL + id, forceSafariVC: false);
      }
    } catch (e) {
      await launch(PinterestFallbackURL + id, forceSafariVC: false);
    }
  }

  Future<void> openTwitchProfile(id) async {
    try {
      bool launched = await launch(TwitchURL + id, forceWebView: false);

      if (!launched) {
        await launch(TwitchFallbackURL + id, forceSafariVC: false);
      }
    } catch (e) {
      await launch(TwitchFallbackURL + id, forceSafariVC: false);
    }
  }

  Future<void> openTinderProfile(id) async {
    if (await canLaunch(TinderURL)) {
      await launch(TinderURL + id, forceWebView: false, enableJavaScript: true);
    } else {
      throw 'Could not launch $TinderURL';
    }
  }

  Future<void> openWeChatProfile(id) async {
    if (await canLaunch(WeChatURL)) {
      await launch(WeChatURL + id, forceWebView: false, enableJavaScript: true);
    } else {
      throw 'Could not launch $WeChatURL';
    }
  }

  Future<void> openSoundCloudProfile(id) async {
    if (await canLaunch(SoundCloudURL)) {
      await launch(SoundCloudURL + id,
          forceWebView: false, enableJavaScript: true);
    } else {
      throw 'Could not launch $SoundCloudURL';
    }
  }

  Future<void> openSpotifyProfile(id) async {
    if (await canLaunch(SpotifyURL)) {
      await launch(SpotifyURL + id,
          forceWebView: false, enableJavaScript: true);
    } else {
      throw 'Could not launch $SpotifyURL';
    }
  }

  Future<void> openTelegramProfile(id) async {
    if (await canLaunch(TelegramURL)) {
      await launch(TelegramURL + id,
          forceWebView: false, enableJavaScript: true);
    } else {
      throw 'Could not launch $TelegramURL';
    }
  }
}

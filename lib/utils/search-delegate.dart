import 'dart:convert';
import 'dart:io';

import 'package:findix_app/Components/nearby-user.dart';
import 'package:findix_app/Pages/profile-page.dart';
import 'package:findix_app/utils/constants.dart';
import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;

class CustomSearchDelegate extends SearchDelegate {
  BuildContext contextPage;
  CustomSearchDelegate({this.contextPage});
  @override
  List<Widget> buildActions(BuildContext context) {
    return [
      IconButton(
        icon: Icon(
          Icons.clear,
          color: Colors.grey,
        ),
        onPressed: () {
          query = '';
        },
      ),
    ];
  }

  @override
  Widget buildLeading(BuildContext context) {
    return IconButton(
      icon: Icon(
        Icons.arrow_back,
        color: Colors.blueAccent,
      ),
      onPressed: () {
        close(context, null);
      },
    );
  }

  @override
  Widget buildSuggestions(BuildContext context) {
    return resultsWidget();
  }

  @override
  Widget buildResults(BuildContext context) {
    return resultsWidget();
  }

  @override
  ThemeData appBarTheme(BuildContext context) {
    assert(context != null);
    final ThemeData theme = Theme.of(context);
    assert(theme != null);
    return theme.copyWith(
      appBarTheme: AppBarTheme(
        brightness: Platform.isIOS ? Brightness.light : Brightness.dark,
        backgroundColor: Colors.white,
      ),
      inputDecorationTheme: InputDecorationTheme(
        border: InputBorder.none,
      ),
    );
  }

  Widget resultsWidget() {
    return FutureBuilder(
      future: query.length > 3 ? getResults() : Future.value(<NearbyUser>[]),
      builder: (BuildContext context, snapshot) {
        if (snapshot.connectionState == ConnectionState.done) {
          print("done");
          if (snapshot.hasData && snapshot.data.length > 0) {
            List<NearbyUser> users = snapshot.data;
            return Container(
              margin: EdgeInsets.only(top: 10),
              child: ListView(
                children: users,
              ),
            );
          } else {
            return Center(
              child: Text(
                query.length > 3
                    ? "No Results"
                    : "Type 3 or more letters or numbers to search",
                style: TextStyle(
                  color: Colors.grey,
                ),
              ),
            );
          }
        } else {
          return LinearProgressIndicator();
        }
      },
    );
  }

  Future<List<NearbyUser>> getResults() async {
    http.Response response =
        await http.post('$baseUrl/user/search', body: {'query': query});
    if (response.statusCode == 200) {
      List users = jsonDecode(response.body)['data']['users'];
      List<NearbyUser> nearbyUsers = users
          .map(
            (user) => NearbyUser(
              firstName: user['firstName'],
              lastName: user['lastName'],
              image: user['profileImage'],
              occupation: user['occupation'],
              onClick: () async {
                await Navigator.push(
                  contextPage,
                  MaterialPageRoute(
                    settings: RouteSettings(
                      name: "NearbyProfilePage",
                    ),
                    builder: (context) => ProfilePage(
                      id: user['_id'],
                      firstName: user['firstName'],
                      lastName: user['lastName'],
                      coverImage: user['coverImage'],
                      facebookId: user['facebookId'],
                      twitterId: user['twitterId'],
                      youtubeId: user['youtubeId'],
                      githubId: user['githubId'],
                      instagramId: user['instagramId'],
                      tiktokId: user['tiktokId'],
                      snapchatId: user['snapchatId'],
                      linkedinId: user['linkedinId'],
                      behanceId: user['behanceId'],
                      whatsAppId: user['whatsAppId'],
                      pinterestId: user['pinterestId'],
                      twitchId: user['twitchId'],
                      tinderId: user['tinderId'],
                      weChatId: user['weChatId'],
                      soundCloudId: user['soundCloudId'],
                      spotifyId: user['spotifyId'],
                      telegramId: user['telegramId'],
                      email: user['email'],
                      website: user['website'],
                      mobileNumber: user['mobileNumber'],
                      isFavorite: user['isFavorite'] ?? false,
                      followers: user['followers'],
                      occupation: user['occupation'],
                      onPop: (fav) {
                        print(fav);
                      },
                    ),
                  ),
                );
              },
            ),
          )
          .toList();
      return nearbyUsers;
    } else {
      return [];
    }
  }
}

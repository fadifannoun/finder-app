import 'package:flutter/material.dart';

// const baseUrl = 'https://findix.herokuapp.com';
const baseUrl = 'https://ferfax.jaztec.xyz';
// const baseUrl = "http://10.0.2.2:9000";
// const baseUrl = "http://192.168.10.119:9000";

const FacebookURL = "https://www.facebook.com/";
const FacebookProtocolURL = "fb://facewebmodal/f?href=$FacebookURL";
const FacebookFallbackURL = "https://facebook.com/";

const InstagramProtocolURL = "instagram://user?username=";
const InstagramFallbackURL = "https://instagram.com/";

const YoutubeURL = "https://youtube.com/channel/";

const TiktokURL = 'https://tiktok.com/@';

const LinkedinURL = "https://linkedin.com/in/";

const TwitterProtocolURL = "twitter://user?screen_name=";
const TwitterFallbackURL = "https://twitter.com/";

const SnapchatURL = "https://snapchat.com/add/";

const GithubURL = "https://github.com/";

const BehanceURL = "https://behance.net/";

const WhatsappURL = "https://wa.me/";
const PinterestURL = "pinterest://user/";
const PinterestFallbackURL = "https://www.pinterest.com/";
const TwitchURL = "twitch://stream/";
const TwitchFallbackURL = "https://twitch.com/";
const TinderURL =
    "https://tinder.com/"; //does not have a public api, not tested
const WeChatURL = "weixin://dl/chat?"; //todo: test when app is not installed
const SoundCloudURL = "soundcloud://"; //not tested
const SpotifyURL = "https://open.spotify.com/user/";
const TelegramURL = "https://t.me/";

const Color primaryBlue = Color(0xFF211764);
const Color secondaryBlue = Color(0xFF595288);

//add social links
//maybe add auth endpoints

import 'package:firebase_analytics/firebase_analytics.dart';
import 'package:firebase_analytics/observer.dart';

class AnalyticsService {
  final FirebaseAnalytics _analytics = FirebaseAnalytics();

  FirebaseAnalyticsObserver getAnalyticsObserver() =>
      FirebaseAnalyticsObserver(analytics: _analytics);

  Future logLogin() async {
    await _analytics.logLogin();
  }

  Future logTabChanged({String tab}) async {
    await _analytics.logEvent(
      name: "tab_changed",
      parameters: {
        "tab": tab,
      },
    );
  }

  Future logVisibility({bool isVisible}) async {
    await _analytics.logEvent(
      name: "visibility",
      parameters: {
        "visible": isVisible,
      },
    );
  }

  Future logSocialConnected({String social}) async {
    await _analytics.logEvent(
      name: "social_connected",
      parameters: {
        "social_media": social,
      },
    );
  }
}

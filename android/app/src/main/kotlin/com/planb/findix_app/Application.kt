package com.planb.findix_app

//import android.annotation.SuppressLint
//import android.app.job.JobInfo
//import android.app.job.JobScheduler
//import android.content.ComponentName
//import android.os.Bundle
import io.flutter.app.FlutterApplication
import io.flutter.plugin.common.PluginRegistry
import io.flutter.plugin.common.PluginRegistry.PluginRegistrantCallback
import com.it_nomads.fluttersecurestorage.FlutterSecureStoragePlugin
import io.flutter.plugins.sharedpreferences.SharedPreferencesPlugin
import io.flutter.view.FlutterMain
import rekab.app.background_locator.LocatorService
//import rekab.app.background_locator.IsolateHolderService

class Application : FlutterApplication(), PluginRegistrantCallback {
//     var TAG = "MainActivity"
//     private var jobScheduler: JobScheduler? = null
//     private var compName: ComponentName? = null
//     private var jobInfo: JobInfo? = null

    override fun onCreate() {
        super.onCreate()
//        startBackgroundTask()
//        IsolateHolderService.setPluginRegistrant(this)
        LocatorService.setPluginRegistrant(this)

        FlutterMain.startInitialization(this)
    }

    override fun registerWith(registry: PluginRegistry?) {

        if (!registry!!.hasPlugin("com.it_nomads.fluttersecurestorage")) {
            FlutterSecureStoragePlugin.registerWith(registry!!.registrarFor("com.it_nomads.fluttersecurestorage"))
        }
        if (!registry!!.hasPlugin("io.flutter.plugins.sharedpreferences")) {
            SharedPreferencesPlugin.registerWith(registry!!.registrarFor("io.flutter.plugins.sharedpreferences"))
        }
    }

//    @SuppressLint("NewApi")
//    fun startBackgroundTask() {
//        jobScheduler = applicationContext.getSystemService(JOB_SCHEDULER_SERVICE) as JobScheduler
//        compName = ComponentName(applicationContext, MyService::class.java)
//        jobInfo = JobInfo.Builder(1, compName!!)
//                .setMinimumLatency(10000) //10 sec interval
//                .setRequiredNetworkType(JobInfo.NETWORK_TYPE_ANY).setRequiresCharging(false).build()
//        jobScheduler!!.schedule(jobInfo!!)
//    }
}